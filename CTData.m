classdef CTData   
    properties
        No          % number of measurement
        Frames      % number of frames
        MF          % multiframe tif
    end
    
    properties (SetAccess = private)
      
    end
    properties (Dependent, SetAccess = private)
        Path        % Path to data
    end
    
    properties 
      DirData = './../data/mouse2_raw';
    end
    
    methods
        function obj = CTData(no)
            obj.No = no;           
        end
        
        function path = get.Path(obj)
            path = fullfile(obj.DirData, sprintf('CR_%.2d.TIF',obj.No));
        end
        
        
        function movplay(obj, colormap)
            if ~exist('colormap','var')
                colormap = @gray;
            end
            implay(CTData.getMov(obj.MF, colormap),90);
        end
                
        
        function obj=setDynRange(obj,min,max)
            obj.MF(obj.MF<min)=min;
            obj.MF(obj.MF>max)=max;
        end 
        
        
         function arrangeWindows(obj)
            N=length(obj);
            mov_handle=findall(0,'tag','spcui_scope_framework');
            screen_size=get(0,'screensize');
            screen_width=screen_size(3);
            screen_height=screen_size(4);
            
            rows = floor(sqrt(N));
            while mod(N,rows) ~= 0
                rows = rows - 1;
            end
            
            cols=N/rows;
            window_width=(screen_width./cols)*0.9;
            window_height=(screen_height./rows)*0.7;
            window_size=min([window_width window_height]);
            
            for iter=1:N
                row_pos=ceil(iter/cols);
                col_pos=iter-((row_pos-1)*cols);
                set(mov_handle(iter),'position',[50+(col_pos-1)*window_size 50+(row_pos-1)*(window_size+150) window_size window_size]);
                %set(mov_handle(iter), 'Name', getString(obj(N-iter+1))); %// set title
            end
        end
    end
    
    methods (Static)
        function playmov(mf, fps,colormap)
            if ~exist('colormap','var')
                colormap = @gray;
            end
            if ~exist('fps','var')
                fps=60;
            end
            implay(CTData.getMov(mf, colormap),fps);
        end
        function obj = loadData(no,dir)
            warning('off','MATLAB:imagesci:tiffmexutils:libtiffErrorAsWarning');
            obj = CTData(no);
            if (~isempty(dir))
                obj.DirData=dir;
            end
            path = obj.Path;
            mf = CTData.readMultiframe(path);
            obj.MF = mf;
            obj.MF=flip(obj.MF,1);
            obj.MF=flip(obj.MF,2);
        end
        
        function  [pks,pks_frame]=getPeaks(timeEvolution,minPeakDist,peakToMeanRatio)
            meanSig=mean(timeEvolution);
            maxSig=max(timeEvolution);
            minPeakHeight=peakToMeanRatio*(maxSig-meanSig)+meanSig;
            [pks, pks_frame]=findpeaks(timeEvolution,'MinPeakDistance',minPeakDist,...
                'MinPeakHeight',minPeakHeight);          
        end
    
        function finalImage = readMultiframe(fileTif)
            % Read in a multiframemov tiff
            % http://www.matlabtips.com/how-to-load-tiff-stacks-fast-really-fast/
            InfoImage=imfinfo(fileTif);
            mImage=InfoImage(1).Width;
            nImage=InfoImage(1).Height;
            NumberImages=length(InfoImage);
            finalImage=zeros(nImage,mImage,NumberImages,'uint16');
            
            TifLink = Tiff(fileTif, 'r');
            for i=1:NumberImages
                TifLink.setDirectory(i);
                finalImage(:,:,i)=TifLink.read();
            end
            TifLink.close();
        end
        
        function converted = convert2uint8(mf)
            minGlobal = min(min(min(mf)));
            maxGlobal = max(max(max(mf)));
            norm = 2^8/double(maxGlobal-minGlobal);
            converted = uint8(double(mf-minGlobal).*norm);
        end
        
        function mov = getMov(mf, colormap)
            % Convert multiframe image in movie
            dim = size(mf);
            if ~isa(mf, 'uint8')
                converted = CTData.convert2uint8(mf);
            else
                converted = mf;
            end
            %             map = repmat(linspace(0,1,2^8)',[1 3]);
            map = colormap(2^8);
            mov = immovie(reshape(converted,[dim(1),dim(2),1,dim(3)]), map);
            
        end
        
    end
end