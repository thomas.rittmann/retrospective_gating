function J=regiongrowing3D(I,x,y,z,reg_maxdist,varargin)
%
% Adapted for 3 dimensions by Thomas Rittmann:
%
% This function performs "region growing" in an image from a specified
% seedpoint (x,y)
%
% J = regiongrowing(I,x,y,t)
%
% I : input image
% J : logical output image of region
% x,y : the position of the seedpoint (if not given uses function getpts)
% t : maximum intensity distance (defaults to 0.2)
%
% The region is iteratively grown by comparing all unallocated neighbouring pixels to the region.
% The difference between a pixel's intensity value and the region's mean,
% is used as a measure of similarity. The pixel with the smallest difference
% measured this way is allocated to the respective region.
% This process stops when the intensity difference between region mean and
% new pixel become larger than a certain treshold (t)
%
% Example:
%
% I = im2double(imread('medtest.png'));
% x=198; y=359;
% J = regiongrowing(I,x,y,0.2);
% figure, imshow(I+J);
%
% Author: D. Kroon, University of Twente

p = inputParser;
p.addParameter('order','min');
p.addParameter('verbose','text');
parse(p,varargin{:});
order_method=p.Results.order;
verbose_level=p.Results.verbose;

global neg_list
if(exist('reg_maxdist','var')==0), reg_maxdist=0.2; end
if(exist('y','var')==0), figure, imshow(I,[]); [y,x]=getpts; y=round(y(1)); x=round(x(1)); end

J = zeros(size(I)); % Output
Isizes = size(I); % Dimensions of input image

reg_mean = single(I(x,y,z)); % The mean of the segmented region
reg_size = single(1); % Number of voxels in region

% Free memory to store neighbours of the (segmented) region
neg_free = single(10000); 
neg_pos=single(0);
neg_list = single(zeros(neg_free,4));
ind_c=0;
indexvec=uint32(zeros(10000,1));
pixdistvec=[];
pixdist=0; % Distance of the region newest pixel to the regio mean

% Neighbor locations (footprint)
neigb=[-1 0 0 ; 1 0 0 ; 0 -1 0;0 1 0; 0 0 1; 0 0 -1];

% Start regiogrowing until distance between regio and posible new pixels become
% higher than a certain treshold
regall=single(zeros(100000,1));
counte=0;
index=uint64(1);
output=[];
fprintf_r('reset')
tic

if strcmp(order_method,'simple');
while(pixdist<reg_maxdist&&reg_size<numel(I))
    counte=counte+1;
    newneighbors();
        method_simple();
        if index>neg_pos
                break;
        end
    voxelupdate();
    info_output();
end

elseif strcmp(order_method,'min')
    while(pixdist<reg_maxdist&&reg_size<numel(I))
    counte=counte+1;
    newneighbors();
        method_min();
        if(neg_pos>30000&&abs(neg_list(index,4)-reg_mean)>reg_maxdist)
                break;
        end
    voxelupdate();
   info_output();
    end
end

fprintf('\n');
J=J>1;

    function newneighbors
        for j=1:6
            % Calculate the neighbour coordinate
            xn = x +neigb(j,1) ;
            yn = y +neigb(j,2) ;
            zn=z+neigb(j,3);
            
            % Check if neighbour is inside or outside the image
            ins=(xn>=1)&&(yn>=1)&&(zn>=1)&&(xn<=Isizes(1))&&(yn<=Isizes(2)) && zn<=Isizes(3);
            
            % Add neighbor if inside and not already part of the segmented area
            if(ins&&(J(xn,yn,zn)==0))
                neg_pos = neg_pos+1;
                neg_list(neg_pos,:) = [xn yn zn I(xn,yn,zn)]; J(xn,yn,zn)=1;
            end
        end
        
        % Add a new block of free memory
        if(neg_pos+10>neg_free), neg_free=neg_free+100000; neg_list((neg_pos+1):neg_free,:)=0; end
    end

    function method_simple
        while(abs(neg_list(index,4)-reg_mean)>reg_maxdist)
            index=index+1;
        end
        
    end
% 
    function method_min
        
        if neg_pos<30000 
            dist = abs(neg_list(1:neg_pos,4)-reg_mean);
            [pixdist, index]=min(dist);
       
        elseif neg_pos>=30000
          if (mod(counte,100000)==0 || abs(neg_list(index+1,4)-reg_mean)>reg_maxdist)
              %tic
              dist = abs(neg_list(1:neg_pos,4)-reg_mean);
              [pixdistvec, indexvec] = sort(dist);
              %toc
              neg_list=neg_list(indexvec,:);
              index=0;
          end
          neg_list(1,4);
          index=index+1;
        end     
    end

%    function method_min
%         dist = abs(neg_list(1:neg_pos,4)-reg_mean);
%         [pixdist, index]=min(dist);    
%     end

    function voxelupdate
        J(x,y,z)=2; reg_size=reg_size+1;
        
        % Calculate the new mean of the region
        %reg_mean= (reg_mean*reg_size + neg_list(index,4))/(reg_size+1);
        %regall(counte)=reg_mean;
        % Save the x and y coordinates of the pixel (for the neighbour add proccess)
        x = neg_list(index,1); y = neg_list(index,2); z=neg_list(index,3);
        
        % Remove the pixel from the neighbour (check) list
        % put neg_pos to list because it is overwritten by new neighbor. 
         %neg_list(index,4)=0.5; 
         neg_list(index,:)=neg_list(neg_pos,:);
         neg_pos=neg_pos-1;
             
    end

    function info_output
        if (mod(counte,10000)==0)
            t=toc;
            fprintf_r('Elapsed time is %.1f s, Region size is %.3e',[t,reg_size]);
            if strcmp(verbose_level,'graphic') || strcmp(verbose_level,'analytic')
                for i =1:4
                figure(i+3);
                imagesc(J(:,:,260+10*i));
                drawnow;
                end
            end
            
            if strcmp(verbose_level,'analytic')
                figure(10);
                plot(regall);
                drawnow;
            end
        end
    end
end
