function [canceled,filtertype,refFrame,axis,tvIter,tvHyper,boxSize] = filterDialog(currentSlice,maxSlice,curAxis)

prompt = {'Select Filter/Modification Action:';'Ref. Slice';'Axis';'TV Iter.';'TV Hyperparam';'Box Size'};
name = 'Filter / Modifications';

formats = struct('type', {}, 'style', {}, 'items', {}, ...
  'format', {}, 'limits', {}, 'size', {});

formats(1,1).span= [1 5];
formats(1,1).type   = 'list';
formats(1,1).style = 'radiobutton';
formats(1,1).items  = {'Subtract reference slice', 'TV Filter','Box Mean Filter'};

formats(2,1).type   = 'edit';
formats(2,1).format = 'integer';
formats(2,1).limits = [1 maxSlice];
formats(2,1).size=[50 20];

formats(2,2).type   = 'list';
formats(2,2).style = 'popupmenu';
formats(2,2).items={'x','y','z'};
formats(2,2).size=[50 20];

formats(2,3).type   = 'edit';
formats(2,3).format = 'integer';
formats(2,3).limits = [1 1000];
formats(2,3).size=[50 20];

formats(2,4).type   = 'edit';
formats(2,4).format = 'integer';
formats(2,4).limits = [1 1000];
formats(2,4).size=[50 20];

formats(2,5).type   = 'edit';
formats(2,5).format = 'integer';
formats(2,5).limits = [1 50];
formats(2,5).size=[50 20];

defaultanswer = {1,currentSlice,curAxis,50,15,3};

[answer, canceled] = inputsdlg(prompt, name, formats, defaultanswer);
filtertype=answer{1};
refFrame=answer{2};
axis=answer{3};
tvIter=answer{4};
tvHyper=answer{5};
boxSize=answer{6};

end

