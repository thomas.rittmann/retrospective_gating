function mat_to_vff(FileName, PathName)

%
% mat_to_vff
% - OR -
% mat_to_vff( workspace_mat_file, 'target_directory_for_the_vff_file')
%

%% Check the input
if nargin ~= 2 
    if nargin ~= 0
      disp( 'Two, or NO input arguments are required.' );
      disp( 'mat_to_vff(FileName, PathName) ---OR--- mat_to_vff');
      disp( '   ');
      return
    end
end

%% Selecting the file
if nargin == 0
    [FileName, PathName] = uigetfile('*.mat','Select the MATLAB data file (*.mat) to be converted in *.vff');
    loaddata = fullfile(PathName, FileName);
    data_file = importdata(loaddata);
elseif nargin == 2
    data_file = FileName;
    FileName = inputname(1);
    FileName = [FileName,'.mat'];    
end

%% Rescaling or NOT
choice = questdlg('Do you want to rescale the data?', ...
	'Rescaling in the range +/- 32k?', ...
	'Yes','No','Enter rescaling factor','No');
% Handle response
switch choice
    case 'Yes'
        % Rescaling --- Multiplying orig data
        max_val_display_vff = 32000;
        min_val = min(data_file(:));
        max_val = max(data_file(:));
        temp = max(abs(min_val), abs(max_val));
        mult_const = abs(max_val_display_vff/temp);
        data_file = data_file .* mult_const;
        f = get(0,'Format');
        format shortG
        msgbox( [' Data was multiplied by ', mat2str(mult_const), ' and is in the range +/- 32k']);
        format(f)
        %MIJ.createImage('REscaled_AIRin_158',REscaled_perm_vol(:,:,400:539),true);
    case 'No'
        disp('Data was not rescaled!')
    case 'Enter rescaling factor'
        prompt={'Enter a rescaling value:'};
        name = 'Rescaling Value';
        defaultans = {'1'};
        answer = inputdlg(prompt,name,[1 21],defaultans);
        data_file = data_file .* str2num(answer{1});
        min_val = min(data_file(:));
        max_val = max(data_file(:));
        f = get(0,'Format');
        format shortG
        msgbox( [' Data was multiplied by ', mat2str(floor(str2num(answer{1}))), ' and is in the range [', mat2str(floor(min_val)), ', ',  mat2str(floor(max_val)), ']']);
        format(f)
end

%% VFF const's
[ ~, name, ~] = fileparts(FileName);
FileName = fullfile([name '.vff']);
save_VFF_data = fullfile(PathName,FileName);
origin = [size(data_file,1)/2 size(data_file,2)/2 size(data_file,3)/2];
spacing = [1 1 1];
title = FileName;
dataType = 'uint8';
bits = 8;
centerofrotation = size(data_file,1)/2;
elementsize = '1.000000';
centralslice = size(data_file,3)/2;
reverseorder = 'no';
dim = size(data_file);
rawsize = prod(dim)*bits/8; % Raw size of the data in bytes

%% Writing VFF
fid = fopen(save_VFF_data,'w','b');% Open the file and write to it
    fprintf(fid,'ncaa;\n');
    fprintf(fid,'rank=3;\n');
    fprintf(fid,'type=raster;\n');
    fprintf(fid,'format=base;\n');%fprintf(fid,'format=slice;\n');
    fprintf(fid,'bits=%d;\n',bits);
    fprintf(fid,'bands=1;\n');
    fprintf(fid,'size=%d %d %d;\n',dim);
    fprintf(fid,'spacing=%f %f %f;\n',spacing); % which is the PixelDimension
    fprintf(fid,'elementsize=%s;\n',elementsize);
    fprintf(fid,'central_slice=%f;\n',centralslice);
    fprintf(fid,'reverse_order=%s;\n',reverseorder);
    fprintf(fid,'origin=%d %d %d;\n',origin);
    fprintf(fid,'rawsize=%d;\n',rawsize);
    fprintf(fid,'data_scale=1;\n');
    fprintf(fid,'title=%s;\n',title);
    fprintf(fid,'date=%s;\n',datestr(now));
    fprintf(fid,'\f\n');
    fwrite(fid,data_file,'uint8',0,'ieee-be'); % fwrite(fid,data,[dataType num2str(bits)]);
fclose(fid);

%% Clear
clear data_file FileName PathName loaddata data_file max_val_display_vff min_val max_val temp mult_const name choice prompt f
clear old_values new_values name ext save_VFF_data origin spacing title dataType bits centerofrotation elementsize centralslice reverseorder





































































