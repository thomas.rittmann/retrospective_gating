function [canceled,radius] = fillDialog()

prompt = {'Sphere radius:'};
name = 'Fill holes with shape';

formats = struct('type', {}, 'style', {}, 'items', {}, ...
  'format', {}, 'limits', {}, 'size', {});
formats(1,1).type   = 'edit';
formats(1,1).format = 'integer';
formats(1,1).size=[60 18];


defaultanswer = {2};

[answer, canceled] = inputsdlg(prompt, name, formats, defaultanswer);
radius=answer{1};

end

