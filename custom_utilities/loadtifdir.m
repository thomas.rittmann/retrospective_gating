function [importedVolume] = loadtifdir(dirpath,naming,start,flip)
            allfiles=dir(fullfile(dirpath,'*.tif'));
            if isempty(allfiles)
                allfiles=dir(fullfile(dirpath,'*.TIFF'));
            end
            NumberImages=numel(allfiles);
            firstTif=fullfile(dirpath,allfiles(1).name);
            InfoImage=imfinfo(firstTif);
            mImage=InfoImage(1).Width;
            nImage=InfoImage(1).Height;
            importedVolume=zeros(nImage,mImage,NumberImages,'uint16');
            for i=1:NumberImages
                fileTif=fullfile(dirpath,sprintf(naming,i-1+start));
                TifLink=Tiff(fileTif,'r');
                importedVolume(:,:,i)=TifLink.read();
                TifLink.close();
            end
            if (flip)
                importedVolume=single(flip(flip(65535-importedVolume,2),3));
            end
end

