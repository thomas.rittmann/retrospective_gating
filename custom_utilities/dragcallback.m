function cost_pos=dragcallback(pos,ylims,xlims)
xmin=xlims(1);
xmax=xlims(2);
new_x=min(pos(:,1));
if (new_x < xmin)
    new_x=xmin;
end
if(new_x>xmax)
    new_x=xmax;
end
cost_pos=[new_x ylims(1); new_x ylims(2)];
end

