function savetiffsnap(tiff_stack,filename)
        t=Tiff(filename,'w');
        setTag(t,'ImageLength',size(tiff_stack,1));
        setTag(t,'ImageWidth',size(tiff_stack,2));
        setTag(t,'Photometric',Tiff.Photometric.MinIsBlack);
        setTag(t,'PlanarConfiguration',Tiff.PlanarConfiguration.Chunky);
        setTag(t,'BitsPerSample',8);
        setTag(t,'SamplesPerPixel',1);
        write(t,tiff_stack);
        close(t);
end
