
prompt = {'Start slice:';'End slice:';'What to cut:'};
name = 'Cut the volume';

formats = struct('type', {}, 'style', {}, 'items', {}, ...
  'format', {}, 'limits', {}, 'size', {});
formats(1,1).type   = 'edit';
formats(1,1).format = 'integer';
formats(1,1).limits = [1 50];
formats(1,1).size=[60 18];

formats(1,2).type   = 'edit';
formats(1,2).format = 'integer';
formats(1,2).limits = [1 50];
formats(1,2).size=[60 18];

formats(2,1).span= [1 2];
formats(2,1).type   = 'list';
formats(2,1).style  = 'radiobutton';
formats(2,1).items  = {'keep inside', 'keep outside'};
defaultanswer = {20, 20,1};

[answer, canceled] = inputsdlg(prompt, name, formats, defaultanswer);