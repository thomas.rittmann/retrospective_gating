function [tosave,canceled] = savedialog()

prompt = { 'Save volume'; 'Save slice'; 'Save ROI mask'; 'Save ROI mask slice';...
    'Save ROI content'; 'Save ROI content slice'};
name = 'Save';

formats = struct('type', {}, 'style', {}, 'items', {}, ...
  'format', {}, 'limits', {}, 'size', {});


formats(1,1).type   = 'check';
formats(1,1).style  = 'checkbox';

formats(1,2).type   = 'check';
formats(1,2).style  = 'checkbox';

formats(2,1).type   = 'check';
formats(2,1).style  = 'checkbox';

formats(2,2).type   = 'check';
formats(2,2).style  = 'checkbox';

formats(3,1).type   = 'check';
formats(3,1).style  = 'checkbox';

formats(3,2).type   = 'check';
formats(3,2).style  = 'checkbox';

defaultanswers={true,false,false,false,false,false};

[tosave, canceled] = inputsdlg(prompt, name, formats,defaultanswers);

end

