function [uint8_output] = pictouint8(voldata,smin,smax)
    voldata=voldata-smin;
    voldata(voldata<0)=0;
    voldata=voldata*255/(smax-smin);
    voldata(voldata>255)=255;
    uint8_output=uint8(voldata);
    uint8_output=255-uint8_output;
    uint8_output(uint8_output==255)=254;
    %uint8_output(uint8_output==0)=1;
end

