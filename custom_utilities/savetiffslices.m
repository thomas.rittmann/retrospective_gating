function savetiffslices(tiff_stack,filename)
    if exist(filename,'dir')~=7
        mkdir(filename);
    end
    for ii = 1 : size(tiff_stack, 3)
        fullfilename= fullfile(filename,sprintf('slice_%03d.TIFF',ii));
        t=Tiff(fullfilename,'w');
        setTag(t,'ImageLength',size(tiff_stack,1));
        setTag(t,'ImageWidth',size(tiff_stack,2));
        setTag(t,'Photometric',Tiff.Photometric.MinIsBlack);
        setTag(t,'PlanarConfiguration',Tiff.PlanarConfiguration.Chunky);
        setTag(t,'BitsPerSample',8);
        setTag(t,'SamplesPerPixel',1);
        write(t,tiff_stack(:,:,ii));
        close(t);
    end
end

