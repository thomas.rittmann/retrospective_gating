function [canceled,first,last,inside,maskval] = cutdialog(defFirst,defLast,defMask)

prompt = {'Start slice:';'End slice:';'What to cut:';'Mask value'};
name = 'Cut the volume';

formats = struct('type', {}, 'style', {}, 'items', {}, ...
  'format', {}, 'limits', {}, 'size', {});
formats(1,1).type   = 'edit';
formats(1,1).format = 'integer';
formats(1,1).limits = [1 defLast];
formats(1,1).size=[60 18];

formats(1,2).type   = 'edit';
formats(1,2).format = 'integer';
formats(1,2).limits = [1 defLast];
formats(1,2).size=[60 18];

formats(2,1).span= [1 2];
formats(2,1).type   = 'list';
formats(2,1).style  = 'radiobutton';
formats(2,1).items  = {'keep inside', 'keep outside'};

formats(3,1).span= [1 2];
formats(3,1).type   = 'edit';
formats(3,1).format = 'integer';
formats(3,1).size=[60 18];
defaultanswer = {defFirst,defLast,1,defMask};

[answer, canceled] = inputsdlg(prompt, name, formats, defaultanswer);
first=answer{1};
last=answer{2};
inside=2-answer{3};
maskval=answer{4};
end

