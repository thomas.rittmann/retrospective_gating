function screen_ratio = screenRatio()
    screen_size=get(0,'screensize');
    screen_width=screen_size(3);
    screen_height=screen_size(4);
    screen_ratio=screen_width/screen_height;
end

