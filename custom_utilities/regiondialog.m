function [canceled,lower,upper] = regiondialog(inmean,instd,outmean,outstd)

prompt = {sprintf('Mean of inner region: %.1f',inmean);...
    sprintf('Standard Dev. of inner region: %.1f',instd);...
    sprintf('Mean of outer region: %.1f',outmean);...
    sprintf('Standard Dev. of outer region: %.1f',outstd);...
    'Lower bound';
    'Upper bound'};
name = 'Region Growth Settings';

formats = struct('type', {}, 'style', {}, 'items', {}, ...
  'format', {}, 'limits', {}, 'size', {});

formats(1,1).type   = 'text';
formats(1,1).style = 'text';
formats(1,1).size=[500 18];

formats(1,2).type   = 'text';
formats(1,2).style = 'text';
formats(1,2).size=[500 18];

formats(2,1).type   = 'text';
formats(2,1).style = 'text';
formats(2,1).size=[500 18];

formats(2,2).type   = 'text';
formats(2,2).style = 'text';
formats(2,2).size=[500 18];

 formats(3,1).type   = 'edit';
 formats(3,1).format   = 'float';
 formats(3,1).size=[60 18];
 
 formats(3,2).type   = 'edit';
 formats(3,2).format   = 'float';
 formats(3,2).size=[60 18];

 signif=2-floor(log(inmean)/log(10));
 signimult=10^signif;
 inmean=round(inmean*signimult)/signimult;
 instd=round(instd*signimult)/signimult;
 defaultanswer = {'','','','',inmean-instd,inmean+instd};

[answer, canceled] = inputsdlg(prompt, name, formats,defaultanswer);
lower=answer{5};
upper=answer{6};

end

