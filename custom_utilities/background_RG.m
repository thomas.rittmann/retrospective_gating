function J=background_RG(I,x,y,z,regLower,regUpper,queue,endqueue,comtoworker,varargin)


p = inputParser;
p.addParameter('order','min');
p.addParameter('verbose','text');
parse(p,varargin{:});
order_method=p.Results.order;
verbose_level=p.Results.verbose;
slicequeue=parallel.pool.PollableDataQueue;
send(queue,slicequeue);
global neg_list
if(exist('y','var')==0), figure, imshow(I,[]); [y,x]=getpts; y=round(y(1)); x=round(x(1)); end

J = zeros(size(I)); % Output
Isizes = size(I); % Dimensions of input image

regLower = single(regLower); 
regUpper = single(regUpper); 
reg_size = single(1); % Number of voxels in region

% Free memory to store neighbours of the (segmented) region
neg_free = single(10000);
neg_pos=single(0);
neg_list = single(zeros(neg_free,4));
ind_c=0;
indexvec=uint32(zeros(10000,1));
pixdistvec=[];
pixdist=0; % Distance of the region newest pixel to the regio mean

% Neighbor locations (footprint)
neigb=[-1 0 0 ; 1 0 0 ; 0 -1 0;0 1 0; 0 0 1; 0 0 -1];

% Start regiogrowing until distance between regio and posible new pixels become
% higher than a certain treshold
regall=single(zeros(100000,1));
counte=0;
index=uint64(1);
output=[];
outputslice=z;
outputax=3;
fprintf_r('reset')
tic
try
if strcmp(order_method,'simple');
    while(reg_size<numel(I))
        counte=counte+1;
        newneighbors();
        method_simple();
        if index>neg_pos
            break;
        end
        voxelupdate();
        info_output();
    end
    
elseif strcmp(order_method,'min')
    while(pixdist<reg_maxdist&&reg_size<numel(I))
        counte=counte+1;
        newneighbors();
        method_min();
        if(neg_pos>30000&&abs(neg_list(index,4)-reg_mean)>reg_maxdist)
            break;
        end
        voxelupdate();
        info_output();
    end
end

fprintf('\n');
J=J>1;
send(endqueue,0);
catch
send(endqueue,1);
end


    function newneighbors
        for j=1:6
            % Calculate the neighbour coordinate
            xn = x +neigb(j,1) ;
            yn = y +neigb(j,2) ;
            zn=z+neigb(j,3);
            
            % Check if neighbour is inside or outside the image
            ins=(xn>=1)&&(yn>=1)&&(zn>=1)&&(xn<=Isizes(1))&&(yn<=Isizes(2)) && zn<=Isizes(3);
            
            % Add neighbor if inside and not already part of the segmented area
            if(ins&&(J(xn,yn,zn)==0))
                neg_pos = neg_pos+1;
                neg_list(neg_pos,:) = [xn yn zn I(xn,yn,zn)]; J(xn,yn,zn)=1;
            end
        end
        
        % Add a new block of free memory
        if(neg_pos+10>neg_free), neg_free=neg_free+100000; neg_list((neg_pos+1):neg_free,:)=0; end
    end

    function method_simple
        while(neg_list(index,4)<regLower || neg_list(index,4)>regUpper)
            index=index+1;
            if(index>neg_pos)
                break;
            end
        end
        
    end
%
    function method_min
        
        if neg_pos<30000
            dist = abs(neg_list(1:neg_pos,4)-reg_mean);
            [pixdist, index]=min(dist);
            
        elseif neg_pos>=30000
            if (mod(counte,100000)==0 || abs(neg_list(index+1,4)-reg_mean)>reg_maxdist)
                %tic
                dist = abs(neg_list(1:neg_pos,4)-reg_mean);
                [pixdistvec, indexvec] = sort(dist);
                %toc
                neg_list=neg_list(indexvec,:);
                index=0;
            end
            neg_list(1,4);
            index=index+1;
        end
    end

%    function method_min
%         dist = abs(neg_list(1:neg_pos,4)-reg_mean);
%         [pixdist, index]=min(dist);
%     end

    function voxelupdate
        J(x,y,z)=2; reg_size=reg_size+1;
        
        % Calculate the new mean of the region
        %reg_mean= (reg_mean*reg_size + neg_list(index,4))/(reg_size+1);
        %regall(counte)=reg_mean;
        % Save the x and y coordinates of the pixel (for the neighbour add proccess)
        x = neg_list(index,1); y = neg_list(index,2); z=neg_list(index,3);
        
        % Remove the pixel from the neighbour (check) list
        % put neg_pos to list because it is overwritten by new neighbor.
        %neg_list(index,4)=0.5;
        neg_list(index,:)=neg_list(neg_pos,:);
        neg_pos=neg_pos-1;
        
    end

    function info_output
        if (mod(counte,10000)==0)
            t=toc;
            fprintf_r('Elapsed time is %.1f s, Region size is %.3e',[t,reg_size]);
            if strcmp(verbose_level,'graphic') || strcmp(verbose_level,'analytic')
                for i =1:4
                    figure(i+3);
                    imagesc(J(:,:,260+10*i));
                    drawnow;
                end
            end
            
            if strcmp(verbose_level,'analytic')
                figure(10);
                plot(regall);
                drawnow;
            end
            
            if strcmp(verbose_level,'background')
                incoming=poll(slicequeue);
                if (~isempty(incoming))
                    outputpara=uint16(incoming);
                    outputslice=outputpara(1);
                    outputax=outputpara(2);
                end
                inds = [{1:size(J,1)},{1:size(J,2)},{1:size(J,3)}];
                inds{outputax} = outputslice;
                send(queue,squeeze(J(inds{:})));
            end
        end
    end
end
