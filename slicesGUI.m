classdef slicesGUI < handle
    properties(Access = public)
        roi_last
        vol_last
        single_fig
        ax_slices
        histo_fig
        single_img
        button_filt
        slice_nr
        sel_axis
        overmax
        overmin
        vol_data
        ax_histo
        liney
        histomin
        histomax
        setmin
        setmax
        histo_line
        bg_mask
        button_reverse
        button_cut
        button_orig
        button_rg
        button_modROI
        button_save
        button_save_single
        edit_min
        edit_max
        histo_img
        slider_min
        slider_max
        slider_histomin
        slider_histomax
        signif
        slider_frame
        button_play
        edit_frame
        select_axes
        hel
        name 
        roi
        colormask
        colormask_orig
        mask_img
        select_display
        display_select=1
        rgStartVal
        roi_orig
        queue
        queuelisten
        roiworker
        pool
        roiworking
        slicequeue
        endqueue
        endlisten
        comtoworker
        button_roi_save
        button_roi_save_single
        infotext
        roisize
        roiml
        infostring
        volumefactor=(20.43/512)^3*1e-3;
        voltext
        rgLower
        rgUpper
        undoable=false;
        vol_orig
        integrated_hist;
        clim;
        clim_last;
        capname;
    end

    methods
        function obj=slicesGUI(vol_data,varargin)
            obj.vol_data=vol_data;
            obj.vol_orig=obj.vol_data;
            p = inputParser;
            p.addParameter('roi',[]);
            parse(p,varargin{:});
            obj.roi=p.Results.roi;
            obj.roi_orig=obj.roi;
        end
        
        function obj=startslicesGUI(obj,capname)
            obj.capname=capname;
            obj.name=inputname(1);
            obj.slice_nr=256;
            obj.overmax=double(max(max(max(obj.vol_data))));
            obj.overmin=double(min(min(min(obj.vol_data))));
            start_slice=obj.slice_nr;
            obj.sel_axis=3;
            screen_ratio=screenRatio();
            
            obj.clim=[1.02*obj.overmin 0.1*obj.overmax];                         
            obj.single_fig=figure(122);
            t_width=0.55*16/9/screen_ratio;
            t_height=0.85;
            act=0;
            mainname=sprintf('RetrospeCT - SlicesViewer: %s',obj.capname);
            set(obj.single_fig,'name',mainname);
            set(obj.single_fig,'NumberTitle','off');
            
            set(0, 'CurrentFigure', obj.single_fig);
            set(obj.single_fig,...
                'Units','normal',...
                'Position',[0.05,0.05,t_width,t_height],...
                'windowscrollWheelFcn',@slicescroll);
            
            
            obj.ax_slices=subplot('Position',[0.05,0.12,0.7389,0.85]);
            bg_color=get(obj.single_fig,'Color');
            obj.single_img=imagesc(obj.vol_data(:,:,obj.slice_nr),obj.clim);
           
            colormap(obj.ax_slices,flipud(gray));
            current_clim=caxis(obj.ax_slices);
            if (~isempty(obj.roi))
                roiplot();
            end
                     
                 
            function roiplot
                hold on;
                obj.colormask = logical(cat(4,obj.roi,zeros(size(obj.roi)),zeros(size(obj.roi))));
                obj.colormask_orig=obj.colormask;
                inds = [{1:size(obj.vol_data,1)},{1:size(obj.vol_data,2)},{1:size(obj.vol_data,3)}];
                inds{4}=1:3;
                inds{obj.sel_axis} = obj.slice_nr;
                
                if (isempty(isgraphics(obj.mask_img)))
                    obj.mask_img=imagesc(squeeze(obj.colormask(inds{:})));
              
                else
                    if(~isvalid(obj.mask_img))
                    obj.mask_img=imagesc(squeeze(obj.colormask(inds{:})));
                    end
                    set(obj.mask_img,'CData',squeeze(obj.colormask(inds{:})));
                end
            %obj.mask_img=imagesc(squeeze(obj.colormask(:,:,obj.slice_nr,:)));
                set(obj.mask_img,'AlphaData',0.2*squeeze(obj.roi(inds{1:3})));
                hold off;
            end
            
            function slicescroll(~,evt)              
                % Callback function for scrolling mouse wheel: scrolls
                % through slices as long as not out of bounds.      
                
                newslice=obj.slice_nr+evt.VerticalScrollCount;
                if(newslice > 0 && newslice <= size(obj.vol_data,obj.sel_axis))
                    obj.slice_nr=newslice;
                    updatePlot();
                end         
            end
            
            %% main controls
            obj.infotext=uicontrol(obj.single_fig,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.82 0.40 0.15 0.05], ...
                'HorizontalAlignment','left',...
                'FontSize',9,...
                'ForeGroundColor','b',...
                'String','');
            
            obj.voltext=uicontrol(obj.single_fig,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.82 0.35 0.15 0.05], ...
                'HorizontalAlignment','left',...
                'FontSize',9,...
                'ForeGroundColor','b',...
                'String','');
                        
            text_maskchoice=uicontrol(obj.single_fig,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.82 0.872 0.15 0.02], ...
                'HorizontalAlignment','left',...
                ...%'FontSize',9,... 
                'ForeGroundColor','k',...
                'String','Selection Tool');
            
            obj.bg_mask = uicontrol(obj.single_fig, ...
                'Style', 'popup',...
                'Units','normal',...
                'Position',[0.82 0.845 0.085 0.03],...
                'String', 'No Mask|Ellipse|Rectangle|Freehand',...
                'Value',1,...
                'Callback',@CB_radio_mask);
            
            obj.button_cut = uicontrol(obj.single_fig,...
                'Style', 'pushbutton', ...
                'String', 'Cut',...
                'Units','normal',...
                'Position', [0.93 0.849 0.05 0.027 ],...
                'Callback',@CB_button_cut);
            
            obj.button_orig = uicontrol(obj.single_fig,...
                'Style', 'pushbutton', ...
                'String', 'Reset to Original',...
                'Units','normal',...
                'Position', [0.82 0.8 0.12 0.03 ],...
                'Callback',@CB_button_orig);
            
            obj.button_reverse=uicontrol(obj.single_fig,...
                'Style', 'pushbutton', ...
                'String', 'Undo last',...
                'Units','normal',...
                'Position', [0.82 0.60 0.07 0.03 ],...
                'Callback',@CB_undo);
            
            obj.button_rg = uicontrol(obj.single_fig,...
                'Style', 'pushbutton', ...
                'String', 'Region Growing',...
                'Units','normal',...
                'Position', [0.82 0.70 0.12 0.03 ],...
                'Callback',@CB_button_rg);     
            
            obj.button_filt = uicontrol(obj.single_fig,...
                'Style', 'pushbutton', ...
                'String', 'Apply filters',...
                'Units','normal',...
                'Position', [0.82 0.75 0.12 0.03 ],...
                'Callback',@CB_button_filter);    
            
            obj.button_save = uicontrol(obj.single_fig,...
                'Style', 'pushbutton', ...
                'String', 'Save',...
                'Units','normal',...
                'Position', [0.82 0.55 0.07 0.03 ],...
                'Callback',@savestart);
            
            obj.button_modROI = uicontrol(obj.single_fig,...
                'Style', 'pushbutton', ...
                'String', 'Modify grown ROI',...
                'Units','normal',...
                'Position', [0.82 0.65 0.12 0.03 ],...
                'Callback',@CB_modROI);
                
            text_display=uicontrol(obj.single_fig,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.895 0.937 0.15 0.03], ...
                'HorizontalAlignment','left',...
                ...%'FontSize',9,... 
                'ForeGroundColor','k',...
                'String','Display');
            
            text_axis=uicontrol(obj.single_fig,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.82 0.937 0.06 0.03], ...
                'HorizontalAlignment','left',...
                ...%'FontSize',9,... 
                'ForeGroundColor','k',...
                'String','Axis');
            
            text_dynrange=uicontrol(obj.single_fig,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.82 0.28 0.15 0.02], ...
                'HorizontalAlignment','left',...
                ...%'FontSize',9,... 
                'ForeGroundColor','k',...
                'String','Dynamic Range Settings');
            
            obj.select_display=uicontrol(obj.single_fig,...
                'Style', 'popup',...
                'Units','normal',...
                'String', 'Image + ROI|Image|ROI',...
                'Position', [0.895 0.9 0.085 0.05],...
                'Value',1,...
                'Callback',@CB_select_display);
            
            
            %% HISTO
            histo_update(1);
            function histo_update(uprange)
                histo_ax=subplot('Position',[0.82,0.18,0.15,0.1]);  
                [hist,edges]=histcounts(obj.vol_data,300);
                hist=hist(3:end);
                edges=edges(3:end);
                hist=hist/max(hist);
                obj.integrated_hist=cumsum(hist);
                obj.integrated_hist=obj.integrated_hist./obj.integrated_hist(end);
                l_boundint=edges(find(obj.integrated_hist>0.02,1));
                r_boundint=edges(find(obj.integrated_hist>0.97,1));
                hwl=edges(find(hist>0.5,1));
                hwr=edges(end+1-find(flip(hist)>0.5,1));
                hwb=hwr-hwl;
                [~,maxin]=max(hist);
                r_hw=edges(maxin)+5*hwb;
                l_hw=edges(maxin)-5*hwb;
                r_b2=edges(end+1-find(flip(hist)>0.02,1));
                l_b2=edges(find(hist>0.02,1));
                r_bound=max([r_boundint,r_b2,r_hw]);
                l_bound=min([l_boundint,l_b2,l_hw]);
                
                if(uprange)
                    obj.clim(1)=squeeze(l_bound);
                    obj.clim(2)=squeeze(r_bound);
                    caxis(obj.ax_slices,obj.clim);
                end
                if(l_bound>obj.clim(1))
                    l_bound=obj.clim(1);
                end
                if(r_bound<obj.clim(2))
                    r_bound=obj.clim(2);
                end
                hist_plot=area(histo_ax,edges(1:end-1),hist);
                set(hist_plot,'FaceColor',[120 120 120]./256);
                bounds=[l_bound r_bound];
                gap=(r_bound-l_bound)./30;
                xlim([l_bound-gap r_bound+gap]);
                ylim([0 1]);
                set(histo_ax,'YTick',[]);
                %set(histo_ax,'xTick',[]);
                ylim_hist=get(histo_ax,'ylim');
                xlim_hist=get(histo_ax,'xlim');
                %gap=round((xlim_hist(2)-xlim_hist(1))/50);
                il_left=imline(histo_ax,[obj.clim(1),obj.clim(1)],[-0.1 1.1]);
                il_right=imline(histo_ax,[obj.clim(2),obj.clim(2)],[-0.1 1.1]);
                il_left.setColor('red');
                il_right.setColor('red');
                setPositionConstraintFcn(il_left,@(p)dragcallback(p,[-0.1 1.1],bounds))
                setPositionConstraintFcn(il_right,@(p)dragcallback(p,[-0.1 1.1],bounds))
                addNewPositionCallback(il_left,@(p)CB_minline(p));
                addNewPositionCallback(il_right,@(p)CB_maxline(p));
            end
                
            function CB_minline(pos)
                obj.clim=[pos(1,1) obj.clim(2)];
                caxis(obj.ax_slices,obj.clim);
            end

            function CB_maxline(pos)
                obj.clim=[obj.clim(1) pos(1,1)];
                caxis(obj.ax_slices,obj.clim);
            end
            %%
            
            function saveforundo
                obj.undoable=true;
                obj.roi_last=obj.roi;
                obj.vol_last=obj.vol_data;
                obj.clim_last=obj.clim;
            end
            
            function CB_modROI(~,~)
                [canceled,radius]=fillDialog;
                if(~canceled)
                    saveforundo();
                    set(obj.single_fig, 'pointer', 'watch');
                    drawnow;
                    shap=strel('sphere',radius);
                    obj.roi=imclose(obj.roi,shap);
                    obj.colormask(:,:,:,1)=obj.roi;
                    updatePlot();
                    volPrint();
                    set(obj.single_fig, 'pointer', 'arrow');
                end
            end
            
            function CB_undo(~,~)
                if obj.undoable
                    set(obj.single_fig, 'pointer', 'watch');
                    drawnow;
                    obj.vol_data=obj.vol_last;
                    obj.roi=obj.roi_last;
                    obj.clim=obj.clim_last;
                    caxis(obj.ax_slices,obj.clim);
                    if(~isempty(obj.roi))
                    obj.colormask(:,:,:,1) =obj.roi;
                    end
                    updatePlot();
                    set(obj.single_fig, 'pointer', 'arrow');
                    obj.undoable=false;
                    histo_update(0);
                else
                    beep;
                end
            end
            
            function CB_radio_mask(src,~)
                masktype=get(src,'Value');
                delete(obj.hel);
                obj.hel=[];
                switch masktype
                    case 1
                        
                    case 2
                        obj.hel=imellipse(obj.ax_slices,[156,156,200,200]);
                    case 3
                        obj.hel=imrect(obj.ax_slices,[156,156,200,200]);
                    case 4
                        obj.hel=imfreehand(obj.ax_slices);
                end
            end
            CB_play_normal=@(src,evt)CB_play(src,evt,1,'Play');
            
            function CB_button_filter(~,~)
                    [canceled,filtertype,refFrame,axis,tvIter,tvHyper,boxSize] = filterDialog(obj.slice_nr,size(obj.vol_data,obj.sel_axis),obj.sel_axis);
                    if(~canceled)
                       saveforundo();
                        
                        set(obj.single_fig, 'pointer', 'watch');
                        drawnow;
                        switch filtertype
                            case 1
                                refRep=[1,1,1];
                                refRep(axis)=size(obj.vol_data,axis);
                                refInd={1:size(obj.vol_data,1),1:size(obj.vol_data,2),1:size(obj.vol_data,3)};
                                refInd{axis}=refFrame;
                                obj.vol_data=obj.vol_data-repmat(obj.vol_data(refInd{:}),refRep);
                                histo_update(1);
                            case 2
                                obj.vol_data=im3DDenoise(obj.vol_data,'TV',tvIter,tvHyper);
                                histo_update(0);
                            case 3
                                obj.vol_data=imboxfilt3(obj.vol_data,[boxSize boxSize boxSize]);
                                histo_update(0);
                        end
                        updatePlot();
                        set(obj.single_fig, 'pointer', 'arrow');
                        drawnow;
                        
                    end
            end
            
            function CB_button_rg(src,~)
                
                if obj.roiworking==1
                    cancel(obj.roiworker);
                    set(obj.button_rg,'BackgroundColor',bg_color);
                    set(obj.button_rg,'String','Region Growing');
                    set(obj.infotext,'String','Region Growing cancelled by user.');
                    obj.roiworking=0;
                else
                axes(obj.ax_slices);    
                obj.comtoworker=com();
                obj.roiworking=1;
                axesind=[1 2 3];
                startpts=[0 0 0];
                startpts(obj.sel_axis)=obj.slice_nr;
                clickpts=[0 0];
                
                waitfor(msgbox('Please select inner region with polygon, double-click to continue')); 
                regRoi=roipoly;
                drawnow;
                rgSlice=obj.vol_data(:,:,obj.slice_nr);
                obj.rgStartVal=mean(mean(rgSlice(regRoi)));
                rgmean=mean(mean(rgSlice(regRoi)));
                rgStDev=std2(rgSlice(regRoi));
                
                waitfor(msgbox('Please select outside region with polygon, double-click to continue')); 
                regOut=roipoly;
                rgOutmean=mean(mean(rgSlice(regOut)));
                rgOutStDev=std2(rgSlice(regOut));
                
                rghistmin=min([rgmean-3*rgStDev rgOutmean-3*rgOutStDev]);
                rghistmax=max([rgmean+3*rgStDev rgOutmean+3*rgOutStDev]);
                rghistofig=figure(100);
                    sr1=subplot(2,1,1);
                    histogram(rgSlice(regRoi));
                    xlim(sr1,[rghistmin rghistmax]);
                    title('Inside region');
                    sr2=subplot(2,1,2);
                    histogram(rgSlice(regOut));
                    xlim(sr2,[rghistmin rghistmax]);
                    title('Outside region');
                
                [canceled,obj.rgLower,obj.rgUpper]=regiondialog(rgmean,rgStDev,rgOutmean,rgOutStDev);
                close 100;
                if(~canceled)    
                    waitfor(msgbox('Please double-click to select start point for region growing.'));
                    [clickvec(:,1), clickvec(:,2)]=getpts(obj.ax_slices);
                    clickpts=clickvec(end,:);
                    drawnow;
                    set(obj.single_fig, 'pointer', 'arrow');
                    startpts(axesind~=obj.sel_axis)=flip(round(clickpts));
                    startt=datestr(now,'HH:MM');
                    obj.infostring=sprintf('Region Growing started [%s]',startt);
                    set(obj.infotext,'String',obj.infostring);
                    drawnow;
                    obj.pool=gcp();
                    obj.queue=parallel.pool.DataQueue;
                    obj.endqueue=parallel.pool.DataQueue;
                    obj.queuelisten=afterEach(obj.queue,@(roi)obj.queueplot(obj.mask_img,roi,obj.comtoworker));
                    obj.endlisten=afterEach(obj.endqueue,@(msg)endroi(msg));
                    if (isempty(obj.roi))
                        obj.roi=zeros(size(obj.vol_data));
                        roiplot();
                    end
                    obj.comtoworker.slice_nr=obj.slice_nr;
                    obj.comtoworker.sel_axis=obj.sel_axis;
                    obj.roiworker=parfeval(obj.pool,@background_RG,1,...
                        obj.vol_data,startpts(1),startpts(2),startpts(3),obj.rgLower,obj.rgUpper,obj.queue,obj.endqueue,obj.comtoworker,...
                        'order','simple','verbose','background');   
                    set(src,'BackgroundColor','red');
                    set(src,'String','Cancel');
                else
                    obj.roiworking=0;
                end
                end
            end
            
            function endroi(msg)
                if (msg==0)
                obj.roi=fetchOutputs(obj.roiworker);
                obj.roi_orig=obj.roi;
                roiplot();
                obj.roisize=sum(sum(sum(obj.roi)));
                    obj.roiml=obj.roisize*obj.volumefactor;
                    obj.infostring=sprintf('Region Growing finished.');
                    set(obj.voltext,'String',sprintf('Volume Size is %dvx (%.3fml)',obj.roisize,obj.roiml));
                    set(obj.infotext,'String',obj.infostring);
                else
                    set(obj.infotext,'String','Region Growing stopped with error.');
                end
                set(obj.button_rg,'BackgroundColor',bg_color);
                set(obj.button_rg,'String','Region Gr.');
                obj.roiworking=0;
            end
            
            function CB_button_cut(~,~)
                if (~isempty(obj.hel))
                    [canceled,fi,la,in,maskVal]=cutdialog(1,size(obj.vol_data,obj.sel_axis),round(min(min(min(obj.vol_data)))-1));
                    if(~canceled)
                        saveforundo();
                        set(obj.single_fig, 'pointer', 'watch');
                        drawnow;
                        cutmask=createMask(obj.hel,obj.single_img);
                        cutdirect=[size(obj.vol_data,1),size(obj.vol_data,2),size(obj.vol_data,3)];
                        cutdirect(obj.sel_axis)=1;
                        shapecut=reshape(cutmask,cutdirect);
                        repdirect=[1,1,1];
                        repdirect(obj.sel_axis)=size(obj.vol_data,obj.sel_axis);
                        shapecut=repmat(shapecut,repdirect);
                        if in
                            toremove=~shapecut;
                        elseif ~in
                            toremove=shapecut;
                        end
                        inds = [{1:size(obj.vol_data,1)},{1:size(obj.vol_data,2)},{1:size(obj.vol_data,3)}];
                        inds{obj.sel_axis} = fi:la;
                        indmat=false(size(obj.vol_data,1),size(obj.vol_data,2),size(obj.vol_data,3));
                        indmat(inds{:})=1;
                        toremove(~indmat)=0;
                        obj.vol_data(toremove)=maskVal;
                        if(~isempty(obj.roi))
                            obj.roi(toremove)=0;
                            obj.colormask= logical(cat(4,obj.roi,zeros(size(obj.roi)),zeros(size(obj.roi))));
                        end
                        updatePlot();
                        set(obj.single_fig, 'pointer', 'arrow');
                        drawnow;
                        histo_update(0);
                    end
                end
                volPrint();
            end
            
            function volPrint
                obj.roisize=sum(sum(sum(obj.roi)));
                obj.roiml=obj.roisize*obj.volumefactor;
                set(obj.voltext,'String',sprintf('Volume Size is %dvx (%.3fml)',obj.roisize,obj.roiml));
            end
            function savestart(~,~)
                %obj.setmin=min(min(min(obj.vol_data)));
                %obj.setmax=max(max(max(obj.vol_data)));
                obj.setmin=obj.clim(1);
                obj.setmax=obj.clim(2);
                [whattosave,canceled]=savedialog();
                if(~canceled)
                    if(whattosave{1})
                        savetoslices('volume');
                    end
                    if(whattosave{2})
                        savesnap('volume');
                    end
                    if(whattosave{3})
                        savetoslices('roi mask');
                    end
                    if(whattosave{4})
                        savesnap('roi mask');
                    end
                    if(whattosave{5})
                        savetoslices('roi content');
                    end
                    if(whattosave{6})
                        savesnap('roi content');
                    end
                end
            end
            
            function savetoslices(type)
                set(obj.single_fig, 'pointer', 'watch');
                drawnow;
                description=sprintf('Save %s as Folder of TIFF Slices',type);
                [outfilename,outfilepath]=uiputfile('*',description,'..\reconstructions\');
                outfilefull=fullfile(outfilepath,outfilename);
                if strcmp(type,'volume')
                    outslices=pictouint8(obj.vol_data,obj.setmin,obj.setmax);  
                end
                if strcmp(type,'roi content')
                    outmult=obj.vol_data.*obj.roi;
                    totmin=min(min(min(outmult)));
                    totmax=max(max(max(outmult)));
                    outslices=pictouint8(outmult,totmin-0.1*(totmax-totmin),totmax+0.1*(totmax-totmin));
                end
                if strcmp(type,'roi mask')
                    outslices=pictouint8(obj.roi,0,1);
                end
                savetiffslices(outslices,outfilefull);
                set(obj.single_fig, 'pointer', 'arrow');
            end
            
            function savesnap(type)
                inds = [{1:size(obj.vol_data,1)},{1:size(obj.vol_data,2)},{1:size(obj.vol_data,3)}];
                inds{obj.sel_axis} = obj.slice_nr;
                if strcmp(type,'volume')
                    selslice=squeeze(obj.vol_data(inds{:}));        
                    outslice=pictouint8(selslice,obj.setmin,obj.setmax);
                elseif strcmp(type,'roi content')
                    selslice=squeeze(obj.vol_data(inds{:}));
                    selroi=squeeze(obj.roi(inds{:}));
                    outslice=pictouint8(selslice.*selroi,obj.setmin,obj.setmax);
                elseif strcmp(type,'roi mask')
                    selslice=squeeze(obj.roi(inds{:}));   
                    outslice=pictouint8(selslice,0,1);
                end
                description=sprintf('Save Single Slice of %s as TIFF',type);
                [outfilename,outfilepath]=uiputfile('.tiff',description,'..\reconstructions\');
                outfilefull=fullfile(outfilepath,outfilename);
                savetiffsnap(outslice,outfilefull);
            end
            
            function CB_button_orig(src,~)
                obj.vol_data=obj.vol_orig;
                obj.colormask=obj.colormask_orig;
                obj.roi=obj.roi_orig;
                updatePlot();
                histo_update(1);
            end                     
         
            CB_button_play_normal=@(src,evt)CB_button_play(src,evt,1);
            
            axis_length=size(obj.vol_data,obj.sel_axis);
            updateControls();
            updateC=@updateControls;
            function updateControls()
                obj.slice_nr=start_slice;
                obj.slider_frame = uicontrol(obj.single_fig, ...
                    'Style', 'slider',...
                    'Units','normal',...
                    'Position',[0.13,0.015,0.54,0.038], ...
                    'Min', 1, ...
                    'Max', axis_length,...
                    'SliderStep',[1/(axis_length-1) 1/(axis_length-1)], ...
                    'Value',obj.slice_nr,...
                    'Callback',@CB_slider_frame);
                
                obj.button_play = uicontrol(obj.single_fig,...
                    'Style', 'pushbutton', ...
                    'String', 'Play',...
                    'Units','normal',...    
                    'Position', [0.05 0.015 0.06 0.04],...
                    'Callback', CB_button_play_normal);
                
                obj.edit_frame=uicontrol(obj.single_fig, ...
                    'Style','edit',...
                    'Units','normal',...
                    'Position', [0.69 0.015 0.04 0.038 ],...
                    'String',sprintf('%d',obj.slice_nr),...
                    'Callback', @CB_edit_frame);
                
                obj.select_axes=uicontrol(obj.single_fig, ...
                    'Style', 'popup',...
                    'Units','normal',...
                    'String', 'x|y|z',...
                    'Position', [0.82 0.9 0.06 0.05],...
                    'Value',obj.sel_axis,...
                    'Callback', @CB_select_axes);
                if(obj.sel_axis==1)
                    set(get(obj.ax_slices,'XLabel'), 'String', 'z' );
                    set(get(obj.ax_slices,'YLabel'), 'String', 'y' );
                end
                
                if(obj.sel_axis==2)
                    set(get(obj.ax_slices,'XLabel'), 'String', 'z' );
                    set(get(obj.ax_slices,'YLabel'), 'String', 'x' );
                end
                
                if(obj.sel_axis==3)
                    set(get(obj.ax_slices,'XLabel'), 'String', 'y' );
                    set(get(obj.ax_slices,'YLabel'), 'String', 'x' );
                end
                
                
            end
            
            
            function CB_slider_frame(src,~)
                obj.slice_nr=round(get(src,'Value'));
                updatePlot();
            end
            
            function CB_edit_frame(src,~)
                obj.slice_nr=str2double(get(src,'String'));
                updatePlot();
            end
                        
            function CB_select_axes(src,~)
                obj.sel_axis=get(src,'Value');
                axis_length=size(obj.vol_data,obj.sel_axis);
                updatePlot();
                updateC();
            end
            
            function CB_select_display(src,~)
                obj.display_select=get(src,'Value');
                if(obj.display_select==1)
                    set(obj.single_img,'Visible','on');
                    if(~isempty(obj.roi))
                    set(obj.mask_img,'Visible','on');
                    alpha_variab();
                    end
                end
                if(obj.display_select==2)
                    set(obj.single_img,'Visible','on');
                    set(obj.mask_img,'Visible','off');
                end
                if(obj.display_select==3)
                    set(obj.single_img,'Visible','off');
                    set(obj.mask_img,'Visible','on');
                    set(obj.mask_img,'AlphaData',1);
                end
            end
            
            function alpha_variab
                inds = [{1:size(obj.vol_data,1)},{1:size(obj.vol_data,2)},{1:size(obj.vol_data,3)}];
                inds{4}=1:3;
                inds{obj.sel_axis} = obj.slice_nr;
                set(obj.mask_img,'AlphaData',0.2.*squeeze(obj.roi(inds{1:3})));
            end
            
            function CB_button_play(src,~,speed)
                if(act==0)
                    set(src,'String','Stop');
                    act=1;
                    while (act==1)
                        drawnow;
                        obj.slice_nr=obj.slice_nr+speed;
                        if(obj.slice_nr>axis_length)
                            obj.slice_nr=obj.slice_nr-axis_length;
                        end
                        updatePlot();
                    end
                    act=0;
                    set(src,'String','Play')
                end
                if(act==1)
                    act=0;
                end
            end
            
            function updatePlot()
                obj.comtoworker.slice_nr=obj.slice_nr;
                obj.comtoworker.sel_axis=obj.sel_axis;
                set(obj.slider_frame,'Value',obj.slice_nr);
                set(obj.edit_frame,'String',sprintf('%d',obj.slice_nr));
                if(obj.sel_axis==3)
                    set(obj.single_img,'CData',obj.vol_data(:,:,obj.slice_nr));
                end
                if(obj.sel_axis==2)
                    set(obj.single_img,'CData',squeeze(obj.vol_data(:,obj.slice_nr,:)));
                end
                if (obj.sel_axis==1)
                    set(obj.single_img,'CData',squeeze(obj.vol_data(obj.slice_nr,:,:)));
                end
                if(~isempty(obj.colormask))
                    inds = [{1:size(obj.vol_data,1)},{1:size(obj.vol_data,2)},{1:size(obj.vol_data,3)}];
                    inds{4}=1:3;
                    inds{obj.sel_axis} = obj.slice_nr;
                    plotmask=squeeze(obj.colormask(inds{:}));
                    set(obj.mask_img,'CData',plotmask);
                    if(obj.display_select==1)
                        set(obj.mask_img,'AlphaData',0.2.*squeeze(obj.roi(inds{1:3})));
                    end
                end
            end
            
            
        end
       
    end
    
    methods(Static)
        function queueplot(img,roi,com)
            if com.established==0
                com.established=1;
                com.WorkerQueue=roi;
            else
            set(img,'AlphaData',0.2*roi);
            set(img,'CData',cat(3,roi,zeros(size(roi)),zeros(size(roi))));
            send(com.WorkerQueue,[com.slice_nr com.sel_axis]);
            end
        end
    end
    
end