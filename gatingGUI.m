classdef gatingGUI < handle
    properties(Access = public)
        dataFolder
        bgColor
        fig_startup
        fig_paramcheck
        figureCaption='RetrospeCT';
        button_load
        button_projection
        button_recon
        rawFrames
        text_progress
        TifFileList
        text_datasetname
        geo
        reconGeo
        param_field
        parts_total
        param_text
        min_pks_frame
        max_pks_frame
        totalImages
        text_fr_in
        text_fr_ex
        bg_method
        view_method
        reconFrames
        angles
        reconAngles
        recon
        slicemanagerAll
        slicemanagerIn
        slicemanagerEx
        renew
        dataName
        view_main_method
        button_view_main
        param_button_viewrecon
        view_all_main
        view_ex_main
        view_in_main
        fig_projection
        peakdst=8
        detrend_sens=50
        movimin
        movimax
        lungmaxmean
        lungminmean
        lungspread
    end

    methods
        function obj=gatingGUI()
            obj.startupScreen();
            InitTIGRE;
        end
        
        function obj=clearMemory(obj)
            delete(obj.fig_startup);
            close all;
            obj.geo=[];
            obj.reconGeo=[];
            obj.min_pks_frame=[];
            obj.max_pks_frame=[];
            obj.rawFrames=[];
            obj.reconFrames=[];
            obj.angles=[];
            obj.reconAngles=[];
            obj.recon=[];
            obj.slicemanagerAll=[];
            obj.slicemanagerIn=[];
            obj.slicemanagerEx=[];
            obj.renew=[];
            obj=obj.startupScreen();
            
        end
        
        function obj=parseGeometry(obj)
            obj.geo.names={'MM_SID';'MM_SOD';'DET_NU';'DET_NV';...
                'DET_CU';'DET_CV';'DET_PU';'DET_PV';...
                'REC_NX';'REC_NY';'REC_NZ';...
                'REC_CX';'REC_CY';'REC_CZ';...
                'REC_PX';'REC_PY';'REC_PZ'};
            fileID = fopen(fullfile(obj.dataFolder,'Recon.prm'),'r');
            A=textscan(fileID,'%s');
                for paramno=1:size(obj.geo.names,1)
                    obj.geo.values(paramno)=str2double(A{1}{find(contains(A{1},obj.geo.names(paramno)))+2});
                end
            fclose(fileID);
        end
        
        function obj=transferGeometry(obj)
            obj.reconGeo.DSD=obj.geo.values(1);
            obj.reconGeo.DSO=obj.geo.values(2);
            obj.reconGeo.nDetector=obj.geo.values(3:4)';
            obj.reconGeo.dDetector=obj.geo.values(7:8)';
            obj.reconGeo.sDetector=obj.reconGeo.nDetector.*obj.reconGeo.dDetector;
            obj.reconGeo.nVoxel=obj.geo.values(9:11)';
            obj.reconGeo.dVoxel=obj.geo.values(15:17)';
            obj.reconGeo.sVoxel=obj.reconGeo.dVoxel.*obj.reconGeo.nVoxel;
            obj.reconGeo.offOrigin=obj.geo.values(12:14)';
            obj.reconGeo.offDetector=-obj.geo.values(5:6)';
            obj.reconGeo.accuracy=0.5;       
        end
            
        function obj=loadAngles(obj)
            raw_angles=dlmread(fullfile(obj.dataFolder,'Angle.prm'));
            if (length(raw_angles)==obj.totalImages)
                obj.angles=raw_angles';
            else
                obj.angles=double(linspace(1/obj.totalImages,1,obj.totalImages)*2*pi);
            end
        end
        
        function obj=recon_paramcheck(obj)
             obj.geo.printnames={'Dist. Source-Detector [mm]';...
                'Dist. Source-Origin [mm]';...
                'Detector Pixels x';...
                'Detector Pixels y';...
                'Detector Offset x [mm]';...
                'Detector Offset y [mm]';...
                'Det. Pixel Size x [mm]';...
                'Det. Pixel Size y [mm]';...
                'Object Voxels x';...
                'Object Voxels y';...
                'Object Voxels z';...
                'Object Offset x [mm]';...
                'Object Offset y [mm]';...
                'Object Offset z [mm]';...
                'Voxel Size x [mm]';...
                'Voxel Size y [mm]';...
                'Voxel Size z [mm]'};
            obj.geo.precision=([0;0;0;0;3;3;4;4;0;0;0;3;3;3;4;4;4]);
            obj.parseGeometry();
            obj.fig_paramcheck=figure(300);
            set(obj.fig_paramcheck,'name','RetrospeCT: Reconstruction Settings');
            set(obj.fig_paramcheck,'Units','normal','Position',[0.3,0.15,0.25,0.6]);
            obj.bgColor=get(obj.fig_paramcheck,'Color');
            set(obj.fig_paramcheck,'menubar','none');
            set(obj.fig_paramcheck,'NumberTitle','off');
            N_param=length(obj.geo.values);
            
            paramtitletext=uicontrol(obj.fig_paramcheck,...
                    'Style','text',...
                    'Units','normal',...
                    'Position',[0.05 0.93 0.40 0.038], ...
                    'HorizontalAlignment','Left',...
                    'BackgroundColor',obj.bgColor,...
                    'FontSize',10,...
                    'FontWeight','bold',...
                    'String','Check geometry parameters:');
                
            text_dataset=uicontrol(obj.fig_paramcheck,...
                    'Style','text',...
                    'Units','normal',...
                    'Position',[0.63 0.90 0.28 0.07], ...
                    'HorizontalAlignment','Left',...
                    'BackgroundColor',obj.bgColor,...
                    'FontSize',8,...
                     ...% 'FontWeight','bold',...
                    'String',sprintf('Projection dataset: %s',obj.dataName));
                
            text_timeneeded=uicontrol(obj.fig_paramcheck,...
                    'Style','text',...
                    'Units','normal',...
                    'Position',[0.63 0.33 0.28 0.1], ...
                    'HorizontalAlignment','Left',...
                    'BackgroundColor',obj.bgColor,...
                    'FontSize',8,...
                    'String','');
                
          %  choice_recon_gate=uicontrol(obj.fig_paramcheck,...
           %     'Style', 'listbox',...
           %     'String', 'all|exhaled|inhaled',...
           %     'Units','normal',...
           %     'Position', [0.63 0.3 0.25 0.08]);
           
            obj.bg_method=uibuttongroup(obj.fig_paramcheck,...
                'Units','normal',...
                'Position',[0.63 0.57 0.25 0.12]);
            bg_all=uicontrol(obj.bg_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Position',[0.1 0.7 0.9 0.2],...
                'Value',1,...
                'String','all frames');
            bg_ex=uicontrol(obj.bg_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Position',[0.1 0.4 0.9 0.2],...
                'String','exhaled');
            bg_in=uicontrol(obj.bg_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Position',[0.1 0.1 0.9 0.2],...
                'String','inhaled');
            
            
            obj.view_method=uibuttongroup(obj.fig_paramcheck,...
                'Units','normal',...
                'Position',[0.63 0.21 0.25 0.12]);
            view_all=uicontrol(obj.view_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Position',[0.1 0.7 0.9 0.2],...
                'Value',get(obj.view_all_main,'Value'),...
                'Enable','off',...
                'String','all frames',...
                'Callback',@all_acti);
            view_ex=uicontrol(obj.view_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Position',[0.1 0.4 0.9 0.2],...
                'Value',get(obj.view_ex_main,'Value'),...
                'Enable','off',...
                'String','exhaled',...
                'Callback',@ex_acti);
            view_in=uicontrol(obj.view_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Enable','off',...
                'Position',[0.1 0.1 0.9 0.2],...
                'Value',get(obj.view_in_main,'Value'),...
                'String','inhaled',...
                'Callback',@in_acti);
            
            function all_acti(~,~)
                set(obj.view_all_main,'Value',1)
            end
            
            function ex_acti(~,~)
                set(obj.view_ex_main,'Value',1)
            end
            
            function in_acti(~,~)
                set(obj.view_in_main,'Value',1)
            end
            
            
            
            
            for paramno=1:N_param
                obj.param_field(paramno)=uicontrol(obj.fig_paramcheck,...
                    'Style','edit',...
                    'Units','normal',...
                    'Position', [0.4 0.92-0.05*paramno 0.1 0.038 ],...
                    'String',sprintf('%.*f',obj.geo.precision(paramno),obj.geo.values(paramno)),...
                    'Callback',@(src,evt)CB_param_field(src,evt,paramno));
                
                obj.param_text=uicontrol(obj.fig_paramcheck,...
                    'Style','text',...
                    'Units','normal',...
                    'Position',[0.05 0.915-0.05*paramno 0.3 0.038], ...
                    'HorizontalAlignment','Left',...
                    'BackgroundColor',obj.bgColor,...
                    'String',obj.geo.printnames(paramno));
            end
            
            button_startrecon = uicontrol(obj.fig_paramcheck,...
                'Style', 'pushbutton', ...
                'String', 'Start Reconstruction',...
                'Units','normal',...
                'Position', [0.63 0.44 0.28 0.1 ],...
                'Interruptible','on',...
                'Callback', @CB_button_startrecon);
            
            obj.param_button_viewrecon=uicontrol(obj.fig_paramcheck,...
                'Style', 'pushbutton', ...
                'String', 'View Volume',...
                'Units','normal',...
                'Position', [0.63 0.08 0.28 0.1 ],...
                'Interruptible','on',...
                'Enable','off',...
                'Callback', @obj.CB_button_viewrecon);
            
            checkWhichVolumes();
            
            function CB_button_startrecon(~,~)
                tic
                set(obj.fig_paramcheck, 'pointer', 'watch');
                drawnow;
                obj.transferGeometry();
                
                if(contains(obj.bg_method.SelectedObject.String,'all frames'))
                    gp=gpuDevice;
                    gpu_memory=gp.AvailableMemory;
                    gpu_max_frames=(gpu_memory-512*512*512*4)/512/512/4*0.75;
                    sparsing=floor(size(obj.rawFrames,3)/gpu_max_frames)+1;
                    if sparsing>1
                    skip_warning = questdlg(sprintf('The selected projection dataset is too large for your GPU memory. \nYou can maximally reconstruct from %.0d projections. \n\nDo you want to continue by only using every %d frames?',round(uint16(gpu_max_frames)),sparsing),...
                        'Warning','Yes','No','Yes');
                    else
                     skip_warning='Yes';
                    end
                    switch skip_warning
                        case 'Yes'
                            obj.reconFrames=single(obj.rawFrames(:,:,1:sparsing:end));
                            obj.reconAngles=obj.angles(1:sparsing:end);
                            obj.recon.All=FDK(obj);
                            obj.renew.All=1;
                        case 'No'
                    end
                end
                
                if(contains(obj.bg_method.SelectedObject.String,'exhaled'))
                    obj.reconFrames=single(obj.rawFrames(:,:,obj.min_pks_frame));
                    obj.reconAngles=obj.angles(obj.min_pks_frame);
                    obj.recon.Ex=FDK(obj);
                    obj.renew.Ex=1;
                end
                
                if(contains(obj.bg_method.SelectedObject.String,'inhaled'))
                    obj.reconFrames=single(obj.rawFrames(:,:,obj.max_pks_frame));
                    obj.reconAngles=obj.angles(obj.max_pks_frame);
                    obj.recon.In=FDK(obj);
                    obj.renew.In=1;
                end
                timerecon=toc;
                set(text_timeneeded,'String',sprintf('reconstructed from %s in %.1f s',obj.bg_method.SelectedObject.String,timerecon));
                set(obj.fig_paramcheck, 'pointer', 'arrow');
                checkWhichVolumes();
                drawnow;
                %close 300
            end
            
            
            function checkWhichVolumes
                if (isfield(obj.recon,'All'))
                    set(obj.param_button_viewrecon,'Enable','on');
                    set(obj.button_view_main,'Enable','on');
                    set(obj.view_all_main,'Enable','on');
                    set(view_all,'Enable','on');
                end
                if (isfield(obj.recon,'Ex'))
                    set(obj.param_button_viewrecon,'Enable','on');
                    set(obj.button_view_main,'Enable','on');
                    set(obj.view_ex_main,'Enable','on');
                    set(view_ex,'Enable','on');
                end
                if (isfield(obj.recon,'In'))
                    set(obj.param_button_viewrecon,'Enable','on');
                    set(obj.button_view_main,'Enable','on');
                    set(obj.view_in_main,'Enable','on');
                    set(view_in,'Enable','on');
                end
            end
            
            
            function CB_param_field(src,~,no)
                newval=str2double(get(src,'String'));
                obj.geo.values(no)=newval;
            end
        end
        
        function CB_button_viewrecon(obj,~,~)
                if(contains(obj.view_main_method.SelectedObject.String,'inhaled'))
                    if (obj.renew.In)
                        obj.slicemanagerIn=slicesGUI(obj.recon.In);
                        obj.renew.In=0;
                    end
                    na=sprintf(' %s, inhaled',obj.dataName);
                    obj.slicemanagerIn=obj.slicemanagerIn.startslicesGUI(na);
                end
                
                if(contains(obj.view_main_method.SelectedObject.String,'exhaled'))
                    if (obj.renew.Ex)
                        obj.slicemanagerEx=slicesGUI(obj.recon.Ex);
                        obj.renew.Ex=0;
                    end
                    na=sprintf(' %s, exhaled',obj.dataName);
                    obj.slicemanagerEx=obj.slicemanagerEx.startslicesGUI(na);
                end
                
                if(contains(obj.view_main_method.SelectedObject.String,'all frames'))
                    if (obj.renew.All)
                        obj.slicemanagerAll=slicesGUI(obj.recon.All);
                        obj.renew.All=0;
                    end
                    na=sprintf(' %s, all frames',obj.dataName);
                    obj.slicemanagerAll=obj.slicemanagerAll.startslicesGUI(na);
                end
               
         end
        
        function obj=startupScreen(obj)
            obj.fig_startup=figure(200);
            set(obj.fig_startup,'Units','normal','Position',[0.3,0.5,0.3,0.23]);
            obj.bgColor=get(obj.fig_startup,'Color');
            set(obj.fig_startup,'menubar','none');
            set(obj.fig_startup,'NumberTitle','off');
            set(obj.fig_startup,'name',obj.figureCaption);
            set(obj.fig_startup,'CloseRequestFcn',@closeRequest);
            
            
            
            function closeRequest(~,~)
                selection = questdlg('Do you really want to close RetrospeCT?','Warning',...
                    'Yes','No','No');
                switch selection
                    case 'Yes'
                        delete(gcf);
                        close all;
                        delete(obj);
                    case 'No'
                        return
                end
            end
            
            %% UI objects / controllers
            obj.button_load = uicontrol(obj.fig_startup,...
                'Style', 'pushbutton', ...
                'String', 'Load Raw Projection Data',...
                'Units','normal',...
                'Position', [0.1 0.6 0.25 0.25 ],...
                'Interruptible','on',...
                'Callback', @CB_button_load);
            
            obj.button_projection = uicontrol(obj.fig_startup,...
                'Style', 'pushbutton', ...
                'String', 'Start Projection Viewer / Gating',...
                'Units','normal',...
                'Position', [0.4 0.6 0.28 0.25 ],...
                'Interruptible','on',...
                'Enable','off',...
                'Callback', @CB_button_projection);
            
            obj.button_recon = uicontrol(obj.fig_startup,...
                'Style', 'pushbutton', ...
                'String', 'Reconstruction',...
                'Units','normal',...
                'Position', [0.72 0.6 0.2 0.25 ],...
                'Interruptible','on',...
                'Enable','off',...
                'Callback', @CB_button_recon);
            
            obj.button_view_main = uicontrol(obj.fig_startup,...
                'Style', 'pushbutton', ...
                'String', 'View Volume',...
                'Units','normal',...
                'Position', [0.3 0.1 0.2 0.25 ],...
                'Interruptible','on',...
                'Enable','off',...
                'Callback', @obj.CB_button_viewrecon);
            
            obj.text_progress=uicontrol(obj.fig_startup,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.1 0.49 0.25 0.08], ...
                'BackgroundColor',obj.bgColor,...
                'HorizontalAlignment','left',...
                'String',''); 
            
            obj.text_fr_ex=uicontrol(obj.fig_startup,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.41 0.49 0.25 0.08], ...
                'BackgroundColor',obj.bgColor,...
                'HorizontalAlignment','left',...
                'HorizontalAlignment','left',...
                'String',''); 
            
            obj.text_fr_in=uicontrol(obj.fig_startup,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.41 0.43 0.25 0.08], ...
                'BackgroundColor',obj.bgColor,...
                'HorizontalAlignment','left',...
                'HorizontalAlignment','left',...
                'String',''); 
            
            obj.text_datasetname=uicontrol(obj.fig_startup,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.1 0.43 0.25 0.08], ...
                'BackgroundColor',obj.bgColor,...
                'HorizontalAlignment','left',...
                'String',''); 
            
            obj.view_main_method=uibuttongroup(obj.fig_startup,...
                'Units','normal',...
                'Position',[0.1 0.1 0.15 0.25]);
            obj.view_all_main=uicontrol(obj.view_main_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Position',[0.1 0.7 0.9 0.25],...
                'Value',1,...
                'Enable','off',...
                'String','all frames');
            obj.view_ex_main=uicontrol(obj.view_main_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Position',[0.1 0.4 0.9 0.25],...
                'Enable','off',...
                'String','exhaled');
            obj.view_in_main=uicontrol(obj.view_main_method,...
                'Style','radiobutton',...
                'Units','normal',...
                'Enable','off',...
                'Position',[0.1 0.1 0.9 0.25],...
                'String','inhaled');
            
           text_author=uicontrol(obj.fig_startup,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.63 0.1 0.35 0.07], ...
                'BackgroundColor',obj.bgColor,...
                'String','by Thomas Rittmann, 2018');
            %% nested function definitions
            
            function progressprint(fr,tot)
                progstring=sprintf('import frame %d of %d',fr,tot);
                set(obj.text_progress,'String',progstring);
                drawnow;
            end
            
            function CB_button_projection(~,~)
                obj=obj.projectionGUI();
            end
            
            function CB_button_recon(~,~)
                obj.recon_paramcheck();
            end
            
            function CB_button_load(~,~)
                % open file dialog:
                
                obj.dataFolder = uigetdir('.\','Please select the folder containing raw projection data...');
                if(obj.dataFolder~=0)
                    set(obj.fig_startup, 'pointer', 'watch');
                    obj.clearMemory();
                    drawnow;
                    tic;
                    loadData();
                    obj.loadAngles();
                    toc;
                    set(obj.fig_startup, 'pointer', 'arrow');
                    drawnow;
                end
            end
            
            function loadData
                warning('off','MATLAB:imagesci:tiffmexutils:libtiffErrorAsWarning');
                % get all TIF files in the folder
                obj.TifFileList=dir(fullfile(obj.dataFolder,'*.TIF'));
                spl=strsplit(obj.dataFolder,'\');
                datcell=spl(end);
                obj.dataName=datcell{1};
                obj.parts_total=length(obj.TifFileList);
                overallcount=0;
                obj.totalImages=0;
                % read number of total frames in advance to can reserve
                % right memory block
                for imgno=1:obj.parts_total
                    imgfile=fullfile(obj.dataFolder,obj.TifFileList(imgno).name);
                    InfoImage=imfinfo(imgfile);
                    obj.totalImages=obj.totalImages+length(InfoImage);
                end
                
                for imgno=1:obj.parts_total
                    imgfile=fullfile(obj.dataFolder,obj.TifFileList(imgno).name);
                    set(obj.text_datasetname,'String',obj.TifFileList(imgno).name);
                    InfoImage=imfinfo(imgfile);
                    NumberImages=length(InfoImage);
                    if imgno==1
                         mImage=InfoImage(1).Width;
                         nImage=InfoImage(1).Height;
                         % reserve memory block (crucial for speed
                         % reasons!!)
                         obj.rawFrames=zeros(nImage,mImage,obj.totalImages,'uint16');
                    end      
  
                    TifLink = Tiff(imgfile, 'r');
                    for i=1:NumberImages
                        overallcount=overallcount+1;
                        if mod(overallcount,20)==0
                            progressprint(overallcount,obj.totalImages);
                        end
                        TifLink.setDirectory(i);
                        obj.rawFrames(:,:,overallcount)=flip(flip(TifLink.read(),1),2);
                    end
                    TifLink.close();
                end
                progressprint(overallcount,obj.totalImages);
                set(obj.button_projection,'Enable','On');
                set(obj.button_recon,'Enable','On');
                set(obj.text_datasetname,'String',obj.dataName);
            end
            
        end
        
        function obj=projectionGUI(obj)
            %close all;
            combmf=obj.rawFrames;
            global minL maxL min_pks max_pks lung_adj line_frame_ov line_frame fwd_speed lungminfluc lungmaxfluc;
            global minrect maxrect lowmindetect upmindetect lowmaxdetect upmaxdetect uplim lolim
          
            minExtend=0;
            screen_ratio=screenRatio();
            % was 8 four mouse 2
            %peakdst=25;
            roi_y=330;
            bin_frame=1; 
            current_fr=1;
            fwd_speed=floor(size(combmf,3)/70);
            all=1:size(combmf,3);
            bin_chosen=all;
            totmin=double(min(min(min(combmf))));
            totmax=double(max(max(max(combmf))));
            gap=double((totmax-totmin)/15);

            clim=[totmin+gap totmax-gap];
            %clim=[2000 6000];
            act=0;
            pic_h=(size(combmf,2));
            proj_width=0.4;
            proj_height=proj_width*0.9/0.85*screen_ratio;

            lungcalc();

            maxL_start=maxL;
            minL_start=minL;
            uplim_start=min([maxL_start+0.05*obj.lungspread, obj.lungmaxmean+0.5*obj.lungspread]);
            lolim_start=max([minL_start-0.05*obj.lungspread,obj.lungminmean-0.5*obj.lungspread]);
            lungcalc();


        %% FIGURES and AXES   

            obj.fig_projection=figure(100);
                set(obj.fig_projection,'Units','normal','Position',[0.05,0.05,0.9,0.85]);
                bg_color=get(obj.fig_projection,'Color');
                set(obj.fig_projection,'NumberTitle','off');
                set(obj.fig_projection,'Name',sprintf('RetrospeCT - ProjectionViewer:  %s',obj.dataName));
                
            ax_projection=subplot('Position',[0.05,(1-proj_height)*0.6,proj_width,proj_height]);
                single_img=imagesc(combmf(:,:,current_fr),clim);
                colormap(ax_projection,gray);
                roi_line=line([0,512],[roi_y, roi_y]);
                set(roi_line,'Color','red');
                set(roi_line,'LineWidth',2);  

            ax_lung_over=subplot('Position',[0.55,0.55,0.4,0.45*proj_height]);
            ax_lung_zoom=subplot('Position',[0.55,(1-proj_height)*0.6,0.4,0.45*proj_height]);
                lungplot();           

                set(gca,'Units','normal');
                supt=annotation('textbox', [0 0.87 1 0.1], ...
                    'String', 'hello, title', ...
                    'EdgeColor', 'none', ...
                    'FontSize',12,...
                    'FontWeight','Bold',...
                    'HorizontalAlignment', 'center');

            histo_ax=subplot('Position',[0.48,0.02,0.15,0.07]);
                [hist,edges]=histcounts(combmf,200);
                hist=hist/max(hist);
                hist_plot=area(histo_ax,edges(1:end-1),hist);
                set(hist_plot,'FaceColor',[120 120 120]./256);
                tot_min=double(min(min(min(combmf))));
                tot_max=double(max(max(max(combmf))));
                xlim([tot_min,tot_max]);
                ylim([0 1]);
                set(histo_ax,'YTick',[]);
                set(histo_ax,'xTick',[]);
                ylim_hist=get(histo_ax,'ylim');
                xlim_hist=get(histo_ax,'xlim');
                gap=round((xlim_hist(2)-xlim_hist(1))/20);
                il_left=imline(histo_ax,[clim(1),clim(1)],[-0.1 1.1]);
                il_right=imline(histo_ax,[clim(2),clim(2)],[-0.1 1.1]);
                il_left.setColor('red');
                il_right.setColor('red');
                line_xlim=[xlim_hist(1)+gap xlim_hist(2)-gap];
                setPositionConstraintFcn(il_left,@(p)dragcallback(p,[-0.1 1.1],line_xlim))
                setPositionConstraintFcn(il_right,@(p)dragcallback(p,[-0.1 1.1],line_xlim))
                addNewPositionCallback(il_left,@(p)CB_minline(p));
                addNewPositionCallback(il_right,@(p)CB_maxline(p));

            function CB_minline(pos)
                clim=[pos(1,1) clim(2)];
                caxis(ax_projection,clim);
            end

            function CB_maxline(pos)
                clim=[clim(1) pos(1,1)];
                caxis(ax_projection,clim);
            end


        %% CONTROLLERS 

            slider_frame = uicontrol(obj.fig_projection, ...
                'Style', 'slider',...
                'Units','normal',...
                'Position',[0.08,0.02,0.2,0.04], ...
                'Min', 1, ...
                'Max', length(bin_chosen),...
                'SliderStep',[1/length(bin_chosen) 1/length(bin_chosen)], ...
                'Value',bin_frame,...
                'Callback',@CB_slider_frame);

            button_play = uicontrol(obj.fig_projection,...
                'Style', 'pushbutton', ...
                'String', 'Play',...
                'Units','normal',...
                'Position', [0.285 0.02 0.03 0.04 ],...
                'Interruptible','on',...
                'Callback', @(src,evt)CB_play(src,evt,1,'Play'));  

            button_fwd = uicontrol(obj.fig_projection, ...
                'Style', 'pushbutton', ...
                'String', 'FWD',...
                'Units','normal',...
                'Position', [0.32 0.02 0.03 0.04 ],...
                'Callback', @(src,evt)CB_play(src,evt,2,'FWD'));   

            edit_fwd=uicontrol(obj.fig_projection, ...
                'Style','edit',...
                'Units','normal',...
                'Position', [0.358 0.02 0.025 0.025 ],...
                'String',sprintf('%d',fwd_speed),...
                'Callback', @CB_edit_fwd_speed);  

            text_fwd=uicontrol(obj.fig_projection,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.356 0.045 0.03 0.018], ...
                'BackgroundColor',bg_color,...
                'String','fw speed'); 

            edit_frame=uicontrol(obj.fig_projection, ...
                'Style','edit',...
                'Units','normal',...
                'Position', [0.05 0.022 0.025 0.03 ],...
                'String',sprintf('%d',current_fr),...
                'Callback', @CB_edit_frame);   

            text_frame=uicontrol(obj.fig_projection,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.05 0.052 0.025 0.018], ...
                'BackgroundColor',bg_color,...
                'String','frame'); 

            slider_roi=uicontrol(obj.fig_projection, ...
                'Style','slider',...
                'Units','normal',...
                'Position', [0.01,(1-proj_height)*0.6-0.02,0.02,proj_height+0.03],...
                'Min',1,'Max',pic_h,...
                'SliderStep',[1/pic_h 1/pic_h], ...
                'Value',pic_h-roi_y+1,...
                'Callback',@CB_slider_roi);

            button_close = uicontrol(obj.fig_projection, ...
                'Style', 'pushbutton', ...
                'String', 'Proceed',...
                'Units','normal', ...
                'Position', [0.9 0.02 0.05 0.05],...
                'Callback', @CB_button_close);

            button_scaleupdate = uicontrol(obj.fig_projection, ...
                'Style', 'pushbutton', ...
                'String', 'Update scaling',...
                'Units','normal', ...
                'Position', [0.83 0.02 0.05 0.05],...
                'Callback', @CB_button_scaleupdate);
                %multiline string not available for pushbutton ui, also not via
                %sprintf or so

            edit_roi = uicontrol(obj.fig_projection, ...
                'Style','edit',...
                'String',sprintf('%d',roi_y), ...
                'Units','normal',...
                'Position',[0.005 0.05 0.03 0.03], ...
                'Callback', @CB_edit_roi);

            edit_minExtend=uicontrol(obj.fig_projection,...
                'Style','edit',...
                'String',sprintf('%d',minExtend), ...
                'Units','normal',...
                'Position',[0.69 0.02 0.02 0.03], ...
                'Callback', @CB_edit_minExtend);

            text_minExtend=uicontrol(obj.fig_projection,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.645 0.02 0.04 0.03], ...
                'BackgroundColor',bg_color,...
                'String',{'det. frames /'; 'inhaled phase'}); 

            edit_peakdst=uicontrol(obj.fig_projection,...
                'Style','edit',...
                'String',sprintf('%d',obj.peakdst), ...
                'Units','normal',...
                'Position',[0.69 0.06 0.02 0.03], ...
                'Callback', @CB_edit_peakdst);     
            
            text_peakdst=uicontrol(obj.fig_projection,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.645 0.06 0.04 0.03], ...
                'BackgroundColor',bg_color,...
                'String',{'min. peak'; 'distance'}); 
            
            text_detrend=uicontrol(obj.fig_projection,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.713 0.06 0.04 0.03], ...
                'BackgroundColor',bg_color,...
                'String',{'detrend'; 'sensitivity'}); 
            
            edit_detrend=uicontrol(obj.fig_projection,...
                'Style','edit',...
                'String',sprintf('%d',obj.detrend_sens), ...
                'Units','normal',...
                'Position',[0.75 0.06 0.02 0.03], ...
                'Callback', @CB_edit_detrend);

            choice_bin=uicontrol(obj.fig_projection,...
                'Style', 'listbox',...
                'String', 'all|inhaled|exhaled',...
                'Units','normal',...
                'Position', [0.40 0.02 0.05 0.05],...
                'Callback', @CB_choice_bin);

            text_roi=uicontrol(obj.fig_projection,...
                'Style','text',...
                'Units','normal',...
                'Position',[0.005 0.08 0.03 0.02], ...
                'BackgroundColor',bg_color,...
                'String','ROI'); 

            updateAll();


        %% CALLBACK FUNCTIONS

            function CB_edit_roi(src,~)
                roi_y=str2double(get(src,'String'));
                updateRoi(roi_y);
                set(slider_roi,'Value',pic_h-roi_y+1);
            end

            function CB_edit_fwd_speed(src,~)
                fwd_speed=str2double(get(src,'String'));
            end

            function CB_edit_minExtend(src,~)
                minExtend=str2double(get(src,'String'));
                lungcalc();
                lungplot();
            end
            
            function CB_edit_detrend(src,~)
                obj.detrend_sens=str2double(get(src,'String'));
                lungcalc();
                lungplot();
            end

            function CB_edit_peakdst(src,~)
                obj.peakdst=str2double(get(src,'String'));
                lungcalc();
                lungplot();
            end

            function CB_edit_frame(src,~)
                current_fr=str2double(get(src,'String'));     
                [~,bin_frame]=min(abs(current_fr-bin_chosen));
                set(slider_frame,'Value',bin_frame);
                updateAll();
            end

            function CB_button_scaleupdate(src,~)
                minL_start=minL;
                maxL_start=maxL;
                lolim_start=lolim;
                uplim_start=uplim;
                lungplot();    
            end

            function CB_choice_bin(src,~)
                bin_sel=get(src,'Value');
                if(bin_sel==1)
                    bin_chosen=all;
                end
                if(bin_sel==2)
                    bin_chosen=obj.max_pks_frame;
                end
                if(bin_sel==3)
                    bin_chosen=obj.min_pks_frame;
                end      
                [~,bin_frame]=min(abs(current_fr-bin_chosen));
                current_fr=bin_chosen(bin_frame);
                set(slider_frame,'Max',length(bin_chosen),...
                    'SliderStep',[1/length(bin_chosen) 1/length(bin_chosen)],...
                    'Value',bin_frame);
                updateAll();
            end

            function CB_slider_roi(src,~)
                roi_y=pic_h-floor(get(src,'Value'))+1;
                updateRoi(roi_y);
                set(edit_roi,'String',roi_y);
            end

            function CB_button_close(~,~)
                act=0;
                close 100;
            end

            function CB_play(src,~,speed,button_str)
                if(act==0)
                    if(speed==2)
                        speed=fwd_speed;
                    end
                    set(src,'String','Stop');
                    act=1;
                    while (act==1)
                        pause(0.03);
                        bin_frame=bin_frame+speed;
                        if(bin_frame>length(bin_chosen))
                            bin_frame=bin_frame-length(bin_chosen);
                        end
                        set(slider_frame,'Value',bin_frame);
                        current_fr=bin_chosen(bin_frame);
                        updateAll();
                    end
                    act=0;
                    set(src,'String',button_str)         
                end
                if(act==1)
                    act=0;
                end
            end

            function CB_slider_frame(src,~)
                bin_frame=round(get(src,'Value'));
                current_fr=bin_chosen(bin_frame);
                updateAll();
            end
            
            function CB_minrect(pos)
                lowmindetect=pos(2);
                upmindetect=pos(2)+pos(4);
                lungdetectpeaks();
                lungplot();
            end
            
            function CB_maxrect(pos)
                lowmaxdetect=pos(2);
                upmaxdetect=pos(2)+pos(4);
                lungdetectpeaks();
                lungplot();
            end

        %% GENERAL FUNCTIONS
        
           
            function lungcalc()
                lungline=squeeze(sum(combmf(roi_y,:,:),2));
                % detrend to suppress low freq. fluctuations
                lungsm=smooth(lungline,obj.detrend_sens);
                lung_adj=lungline-lungsm;
                minL=min(lung_adj);     
                maxL=max(lung_adj);
                obj.movimin=movmin(lung_adj,1.5*obj.peakdst);
                obj.movimax=movmax(lung_adj,1.5*obj.peakdst);
                obj.lungmaxmean=median(obj.movimax);
                obj.lungminmean=median(obj.movimin);
                obj.lungspread=obj.lungmaxmean-obj.lungminmean;
                lungminfluc=max([4*mad(obj.movimin),0.1*obj.lungspread]);
                lungmaxfluc=max([4*mad(obj.movimax),0.1*obj.lungspread]);
                uplim=min([maxL+0.05*obj.lungspread, obj.lungmaxmean+0.5*obj.lungspread]);
                lolim=max([minL-0.05*obj.lungspread,obj.lungminmean-0.5*obj.lungspread]);
             
                
                lowmaxdetect=obj.lungmaxmean-lungmaxfluc;
                upmaxdetect=min([obj.lungmaxmean+lungmaxfluc,uplim]);
                lowmindetect=max([obj.lungminmean-lungminfluc,lolim]);
                upmindetect=obj.lungminmean+lungminfluc;
                lungdetectpeaks();
            end
              
            function lungdetectpeaks()
                
                [min_pks, obj.min_pks_frame]=findpeaks(-lung_adj,'MinPeakDistance',obj.peakdst,...
                    'MinPeakHeight',-upmindetect);   
                obj.min_pks_frame(min_pks>-lowmindetect)=[];
                min_pks(min_pks>-lowmindetect)=[];
                
                [max_pks, obj.max_pks_frame]=findpeaks(lung_adj,'MinPeakDistance',obj.peakdst,...
                    'MinPeakHeight',lowmaxdetect);   
                obj.max_pks_frame(max_pks>upmaxdetect)=[];
                max_pks(max_pks>upmaxdetect)=[];
                
                %[max_pks,obj.max_pks_frame]=CTData.getPeaks(lung_adj,obj.peakdst,0.1);
                %[min_pks,obj.min_pks_frame]=CTData.getPeaks(-lung_adj,obj.peakdst,0.1);
                subts=(0:minExtend);
                if (length(obj.min_pks_frame)>3)
                    obj.min_pks_frame(1)=[];
                    obj.min_pks_frame=reshape(repmat(obj.min_pks_frame,[1 minExtend+1])-subts,[size(obj.min_pks_frame,1)*(minExtend+1) 1]);
                    obj.min_pks_frame=obj.min_pks_frame(obj.min_pks_frame>0);
                    min_pks=lung_adj(obj.min_pks_frame);
                end
                
                set(obj.text_fr_ex,'String',sprintf('frames exhaled: %d',length(obj.min_pks_frame)));
                set(obj.text_fr_in,'String',sprintf('frames inhaled: %d',length(obj.max_pks_frame)));
            end
            
            
            
            
             function cost_pos=dragrectcallback(pos,xlims,lolim,uplim)
                newlow=pos(2);  
                if (pos(2)+pos(4) > uplim)
                    newlow=uplim-pos(4);
                end
                
                if(pos(2)<lolim)
                    newlow=lolim;
                end
                
                cost_pos=[xlims(1) newlow xlims(2) pos(4)];
             end

            
            
            function lungplot()
                axes(ax_lung_zoom);
                    plot(ax_lung_zoom,lung_adj);
                    hold on;
                    scatter(obj.max_pks_frame,max_pks);
                    scatter(obj.min_pks_frame,min_pks);
                    hold off;
                    line_frame=line([current_fr,current_fr],[minL_start, maxL_start]);
                    set(line_frame,'Color','red');
                    set(line_frame,'x',[current_fr, current_fr]);
                    xlim(ax_lung_zoom,[current_fr-70 current_fr+70]);   
                    set(ax_lung_zoom,'ylim',[lolim uplim]);

                axes(ax_lung_over);
                    plot(ax_lung_over,lung_adj); 
                   % hold on;
                    %plot(ax_lung_over,obj.movimin);
                    %hold off;
                    line_frame_ov=line([current_fr,current_fr],[minL_start, maxL_start]);
                    set(line_frame_ov,'x',[current_fr, current_fr]);
                    set(line_frame_ov,'Color','red');  
                    set(ax_lung_over,'ylim',[lolim_start uplim_start],'xlim',[1, length(lung_adj)]);
                    
                    minrect=imrect(ax_lung_over,[-2, lowmindetect,length(lung_adj)+4,upmindetect-lowmindetect]);
                    minrect.setColor([1 0.6 0]);
                    setPositionConstraintFcn(minrect,@(p)dragrectcallback(p,[-2 length(lung_adj)+4],lolim_start, uplim_start))
                    addNewPositionCallback(minrect,@(p)CB_minrect(p));
                    
                    maxrect=imrect(ax_lung_over,[-2, lowmaxdetect,length(lung_adj)+4,upmaxdetect-lowmaxdetect]);
                    maxrect.setColor([0 0.6 0]);
                    setPositionConstraintFcn(maxrect,@(p)dragrectcallback(p,[-2 length(lung_adj)+4],lolim_start, uplim_start))
                    addNewPositionCallback(maxrect,@(p)CB_maxrect(p));
                    
            end 
              
           
            function updateRoi(roi_y)
                lungcalc();
                set(roi_line,'y',[roi_y,roi_y]);
                lungplot();    
            end

            function updateAll()
                %suptitle(sprintf('Current frame: %d / %d',current_fr,length(combmf)));
                set(supt,'String',sprintf('Current frame: %d / %d',current_fr,length(combmf)));
                set(single_img,'CData',combmf(:,:,current_fr));
                %plot(lung_adj((current_fr-50):(current_fr+50)));

                axes(ax_lung_zoom);
                set(line_frame,'x',[current_fr, current_fr]);
                xlim(ax_lung_zoom,[current_fr-70 current_fr+70]);
                axes(ax_lung_over);
                set(line_frame_ov,'x',[current_fr, current_fr]);
                set(edit_frame,'String',sprintf('%d',current_fr));
            end

        end
        
        function [res,errorL2]=FDK(obj,varargin)
            
            %--------------------------------------------------------------------------
            geo=obj.reconGeo;
            angles=obj.reconAngles;
            
            
            [filter,parker]=parse_inputs(obj.reconFrames,obj.reconGeo,angles,varargin);
            obj.reconGeo.filter=filter;

            %Input is data,obj.reconGeosize,angles

            if size(obj.reconGeo.offDetector,2)==1
                offset=repmat(obj.reconGeo.offDetector,[1 length(angles)]);
            else
                offset=obj.reconGeo.offDetector;
            end


            %% Weight
            obj.reconFrames=permute(obj.reconFrames,[2 1 3]);

            for ii=1:length(angles)

                us = ((-obj.reconGeo.nDetector(1)/2+0.5):1:(obj.reconGeo.nDetector(1)/2-0.5))*obj.reconGeo.dDetector(1) + offset(1,ii);
                vs = ((-obj.reconGeo.nDetector(2)/2+0.5):1:(obj.reconGeo.nDetector(2)/2-0.5))*obj.reconGeo.dDetector(2) + offset(2,ii);
                [uu,vv] = meshgrid(us,vs); %detector

                %Create weight according to each detector element
                w = (obj.reconGeo.DSD)./sqrt((obj.reconGeo.DSD)^2+uu.^2 + vv.^2);

                %Multiply the weights with obj.reconFramesection data
                obj.reconFrames(:,:,ii) = obj.reconFrames(:,:,ii).*w';
            end

            % filter call
            
            filtering(obj,parker); % Not sure if offsets are good in here

            %RMFIELD Remove fields from a structure array.
            
            obj.reconGeo=rmfield(obj.reconGeo,'filter');
            %% backproject
            res=Atb((obj.reconFrames),obj.reconGeo,angles); % Weighting is inside


            if nargout>1
                 error=obj.reconFrames-Ax(res,obj.reconGeo,angles);
                 errorL2=norm(error(:));
            end

            
            %nested funct

            function [filter, parker]=parse_inputs(proj,geo,alpha,argin)
            opts=     {'filter','parker'};
            defaults=ones(length(opts),1);
            % Check inputs
            nVarargs = length(argin);
            if mod(nVarargs,2)
                error('CBCT:FDK:InvalidInput','Invalid number of inputs')
            end

            % check if option has been passed as input
            for ii=1:2:nVarargs
                ind=find(ismember(opts,lower(argin{ii})));
                if ~isempty(ind)
                    defaults(ind)=0;
                end
            end

            for ii=1:length(opts)
                opt=opts{ii};
                default=defaults(ii);
                % if one option isnot default, then extranc value from input
                if default==0
                    ind=double.empty(0,1);jj=1;
                    while isempty(ind)
                        ind=find(isequal(opt,lower(argin{jj})));
                        jj=jj+1;
                    end
                     if isempty(ind)
                        error('CBCT:FDK:InvalidInput',['Optional parameter "' argin{jj} '" does not exist' ]); 
                    end
                    val=argin{jj};
                end

                switch opt
                    % % % % % % % Verbose
                    case 'parker'
                        if default
                            parker=0;
                        else
                            parker=val;
                        end

                    case 'filter'
                        if default
                            filter='ram-lak';
                        else
                            if  ~ischar( val)
                                error('CBCT:FDK:InvalidInput','Invalid filter')
                            end
                            filter=val;
                        end

                    otherwise
                        error('CBCT:FDK:InvalidInput',['Invalid input name:', num2str(opt),'\n No such option in FAK()']);
                end
            end

            end
        end
        
        
        %%filtering
        
        
        function filtering(obj,parker)
            geo=obj.reconGeo;
            angles=obj.reconAngles;
            if parker
                obj.reconFrames = permute(ParkerWeight(permute(obj.reconFrames,[2 1 3]),geo,angles,parker),[2 1 3]);
            end 

            filt_len = max(64,2^nextpow2(2*geo.nDetector(1)));
            [ramp_kernel] = ramp_flat(filt_len);

            d = 1; % cut off (0~1)
            [filt] = Filter(geo.filter, ramp_kernel, filt_len, d);
            filt = repmat(filt',[1 geo.nDetector(2)]);

            for ii=1:length(angles)

                fproj = (zeros(filt_len,geo.nDetector(2),'single'));

                fproj(filt_len/2-geo.nDetector(1)/2+1:filt_len/2+geo.nDetector(1)/2,:) = obj.reconFrames(:,:,ii);

                fproj = fft(fproj);   

                fproj = fproj.*filt;

                fproj = (real(ifft(fproj)));


                obj.reconFrames(:,:,ii) = fproj(end/2-geo.nDetector(1)/2+1:end/2+geo.nDetector(1)/2,:)/2/geo.dDetector(1)*(2*pi/  length(angles)   )/2*(geo.DSD/geo.DSO);


            end

            obj.reconFrames=permute(obj.reconFrames,[2 1 3]);
            

            function [h, nn] = ramp_flat(n)
            nn = [-(n/2):(n/2-1)]';
            h = zeros(size(nn),'single');
            h(n/2+1) = 1 / 4;
            odd = mod(nn,2) == 1;
            h(odd) = -1 ./ (pi * nn(odd)).^2;
            end


            function [filt] = Filter(filter, kernel, order, d)

            f_kernel = abs(fft(kernel))*2;
            filt = f_kernel(1:order/2+1)';
            w = 2*pi*(0:size(filt,2)-1)/order;   % frequency axis up to Nyquist 

            switch lower(filter)
                case 'ram-lak'
                    % Do nothing
                case 'shepp-logan'
                    % be careful not to divide by 0:
                    filt(2:end) = filt(2:end) .* (sin(w(2:end)/(2*d))./(w(2:end)/(2*d)));
                case 'cosine'
                    filt(2:end) = filt(2:end) .* cos(w(2:end)/(2*d));
                case 'hamming'  
                    filt(2:end) = filt(2:end) .* (.54 + .46 * cos(w(2:end)/d));
                case 'hann'
                    filt(2:end) = filt(2:end) .*(1+cos(w(2:end)./d)) / 2;
                otherwise
                    disp(filter);
                    error('Invalid filter selected.');
            end

            filt(w>pi*d) = 0;                      % Crop the frequency response
            filt = [filt , filt(end-1:-1:2)];    % Symmetry of the filter
            return

            end
        end
    end
end