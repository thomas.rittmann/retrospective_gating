% working with nested functions because: 
% When working with large data sets, ...
% be aware that MATLAB makes a temporary copy of an input variable if the called ...
% function modifies its value. This temporarily doubles the memory required to store the array, ...
% which causes MATLAB to generate an error if sufficient memory is not available.
% One way to use less memory in this situation is to use nested functions


%% EDIT ROI RESPONSE !!
function projectionGUI(combmf)
    %close all;
    global minL maxL min_pks max_pks min_pks_frame max_pks_frame lung_adj line_frame_ov line_frame fwd_speed;
    minExtend=0;
    screen_ratio=screenRatio();
    % was 8 four mouse 2
    %peakdst=25;
    peakdst=8;
    roi_y=330;
    bin_frame=1; 
    current_fr=1;
    fwd_speed=floor(size(combmf,3)/70);
    all=1:size(combmf,3);
    bin_chosen=all;
    totmin=double(min(min(min(combmf))));
    totmax=double(max(max(max(combmf))));
    gap=double((totmax-totmin)/15);
    
    clim=[totmin+gap totmax-gap];
    %clim=[2000 6000];
    act=0;
    pic_h=(size(combmf,2));
    proj_width=0.4;
    proj_height=proj_width*0.9/0.85*screen_ratio;
       
    lungcalc();
    
    maxL_start=maxL;
    minL_start=minL;
    
    
%% FIGURES and AXES   

    single_fig=figure(100);
        set(single_fig,'Units','normal','Position',[0.05,0.05,0.9,0.85]);
        bg_color=get(single_fig,'Color');
           
    ax_projection=subplot('Position',[0.05,(1-proj_height)*0.6,proj_width,proj_height]);
        single_img=imagesc(combmf(:,:,current_fr),clim);
        colormap(ax_projection,gray);
        roi_line=line([0,512],[roi_y, roi_y]);
        set(roi_line,'Color','red');
        set(roi_line,'LineWidth',2);  
   
    ax_lung_over=subplot('Position',[0.55,0.55,0.4,0.45*proj_height]);
    ax_lung_zoom=subplot('Position',[0.55,(1-proj_height)*0.6,0.4,0.45*proj_height]);
        lungplot();           
        
        set(gca,'Units','normal');
        supt=annotation('textbox', [0 0.87 1 0.1], ...
            'String', 'hello, title', ...
            'EdgeColor', 'none', ...
            'FontSize',12,...
            'FontWeight','Bold',...
            'HorizontalAlignment', 'center');
    
    histo_ax=subplot('Position',[0.48,0.02,0.15,0.07]);
        [hist,edges]=histcounts(combmf,200);
        hist=hist/max(hist);
        hist_plot=area(histo_ax,edges(1:end-1),hist);
        set(hist_plot,'FaceColor',[120 120 120]./256);
        tot_min=double(min(min(min(combmf))));
        tot_max=double(max(max(max(combmf))));
        xlim([tot_min,tot_max]);
        ylim([0 1]);
        set(histo_ax,'YTick',[]);
        set(histo_ax,'xTick',[]);
        ylim_hist=get(histo_ax,'ylim');
        xlim_hist=get(histo_ax,'xlim');
        gap=round((xlim_hist(2)-xlim_hist(1))/20);
        il_left=imline(histo_ax,[clim(1),clim(1)],[-0.1 1.1]);
        il_right=imline(histo_ax,[clim(2),clim(2)],[-0.1 1.1]);
        il_left.setColor('red');
        il_right.setColor('red');
        setPositionConstraintFcn(il_left,@(p)dragcallback(p,[-0.1 1.1],xlim_hist))
        setPositionConstraintFcn(il_right,@(p)dragcallback(p,[-0.1 1.1],xlim_hist))
        addNewPositionCallback(il_left,@(p)CB_minline(p));
        addNewPositionCallback(il_right,@(p)CB_maxline(p));
        
    function CB_minline(pos)
        clim=[pos(1,1) clim(2)];
        caxis(ax_projection,clim);
    end

    function CB_maxline(pos)
        clim=[clim(1) pos(1,1)];
        caxis(ax_projection,clim);
    end
    
    
%% CONTROLLERS 
 
    slider_frame = uicontrol(single_fig, ...
        'Style', 'slider',...
        'Units','normal',...
        'Position',[0.08,0.02,0.2,0.04], ...
        'Min', 1, ...
        'Max', length(bin_chosen),...
        'SliderStep',[1/length(bin_chosen) 1/length(bin_chosen)], ...
        'Value',bin_frame,...
        'Callback',@CB_slider_frame);
    
    button_play = uicontrol(single_fig,...
        'Style', 'pushbutton', ...
        'String', 'Play',...
        'Units','normal',...
        'Position', [0.285 0.02 0.03 0.04 ],...
        'Interruptible','on',...
        'Callback', @(src,evt)CB_play(src,evt,1,'Play'));  
        
    button_fwd = uicontrol(single_fig, ...
        'Style', 'pushbutton', ...
        'String', 'FWD',...
        'Units','normal',...
        'Position', [0.32 0.02 0.03 0.04 ],...
        'Callback', @(src,evt)CB_play(src,evt,2,'FWD'));   
     
    edit_fwd=uicontrol(single_fig, ...
        'Style','edit',...
        'Units','normal',...
        'Position', [0.358 0.02 0.025 0.025 ],...
        'String',sprintf('%d',fwd_speed),...
        'Callback', @CB_edit_fwd_speed);  
    
    text_fwd=uicontrol(single_fig,...
        'Style','text',...
        'Units','normal',...
        'Position',[0.356 0.045 0.03 0.018], ...
        'BackgroundColor',bg_color,...
        'String','fw speed'); 
    
    edit_frame=uicontrol(single_fig, ...
        'Style','edit',...
        'Units','normal',...
        'Position', [0.05 0.022 0.025 0.03 ],...
        'String',sprintf('%d',current_fr),...
        'Callback', @CB_edit_frame);   
    
    text_frame=uicontrol(single_fig,...
        'Style','text',...
        'Units','normal',...
        'Position',[0.05 0.052 0.025 0.018], ...
        'BackgroundColor',bg_color,...
        'String','frame'); 
        
    slider_roi=uicontrol(single_fig, ...
        'Style','slider',...
        'Units','normal',...
        'Position', [0.01,(1-proj_height)*0.6-0.02,0.02,proj_height+0.03],...
        'Min',1,'Max',pic_h,...
        'SliderStep',[1/pic_h 1/pic_h], ...
        'Value',pic_h-roi_y+1,...
        'Callback',@CB_slider_roi);
               
    button_close = uicontrol(single_fig, ...
        'Style', 'pushbutton', ...
        'String', 'Proceed',...
        'Units','normal', ...
        'Position', [0.9 0.02 0.05 0.05],...
        'Callback', @CB_button_close);
    
    button_scaleupdate = uicontrol(single_fig, ...
        'Style', 'pushbutton', ...
        'String', 'Update scaling',...
        'Units','normal', ...
        'Position', [0.75 0.02 0.05 0.05],...
        'Callback', @CB_button_scaleupdate);
        %multiline string not available for pushbutton ui, also not via
        %sprintf or so
    
    edit_roi = uicontrol(single_fig, ...
        'Style','edit',...
        'String',sprintf('%d',roi_y), ...
        'Units','normal',...
        'Position',[0.005 0.05 0.03 0.03], ...
        'Callback', @CB_edit_roi);
    
    edit_minExtend=uicontrol(single_fig,...
        'Style','edit',...
        'String',sprintf('%d',minExtend), ...
        'Units','normal',...
        'Position',[0.69 0.02 0.02 0.03], ...
        'Callback', @CB_edit_minExtend);
     
    text_minExtend=uicontrol(single_fig,...
        'Style','text',...
        'Units','normal',...
        'Position',[0.645 0.02 0.04 0.03], ...
        'BackgroundColor',bg_color,...
        'String',{'det. frames /'; 'inhaled phase'}); 
    
    edit_peakdst=uicontrol(single_fig,...
        'Style','edit',...
        'String',sprintf('%d',peakdst), ...
        'Units','normal',...
        'Position',[0.69 0.06 0.02 0.03], ...
        'Callback', @CB_edit_peakdst);
    
    text_minExtend=uicontrol(single_fig,...
        'Style','text',...
        'Units','normal',...
        'Position',[0.645 0.06 0.04 0.03], ...
        'BackgroundColor',bg_color,...
        'String',{'min. peak'; 'distance'}); 
    
    choice_bin=uicontrol(single_fig,...
        'Style', 'listbox',...
        'String', 'all|inhaled|exhaled',...
        'Units','normal',...
        'Position', [0.40 0.02 0.05 0.05],...
        'Callback', @CB_choice_bin);
    
    text_roi=uicontrol(single_fig,...
        'Style','text',...
        'Units','normal',...
        'Position',[0.005 0.08 0.03 0.02], ...
        'BackgroundColor',bg_color,...
        'String','ROI'); 
               
    updateAll();
    
    
%% CALLBACK FUNCTIONS

    function CB_edit_roi(src,~)
        roi_y=str2double(get(src,'String'));
        updateRoi(roi_y);
        set(slider_roi,'Value',pic_h-roi_y+1);
    end

    function CB_edit_fwd_speed(src,~)
        fwd_speed=str2double(get(src,'String'));
    end

    function CB_edit_minExtend(src,~)
        minExtend=str2double(get(src,'String'));
        lungcalc();
        lungplot();
    end

    function CB_edit_peakdst(src,~)
        peakdst=str2double(get(src,'String'));
        lungcalc();
        lungplot();
    end

    function CB_edit_frame(src,~)
        current_fr=str2double(get(src,'String'));     
        [~,bin_frame]=min(abs(current_fr-bin_chosen));
        set(slider_frame,'Value',bin_frame);
        updateAll();
    end

    function CB_button_scaleupdate(src,~)
        minL_start=minL;
        maxL_start=maxL;
        lungplot();    
    end

    function CB_choice_bin(src,~)
        bin_sel=get(src,'Value');
        if(bin_sel==1)
            bin_chosen=all;
        end
        if(bin_sel==2)
            bin_chosen=max_pks_frame;
        end
        if(bin_sel==3)
            bin_chosen=min_pks_frame;
        end      
        [~,bin_frame]=min(abs(current_fr-bin_chosen));
        current_fr=bin_chosen(bin_frame);
        set(slider_frame,'Max',length(bin_chosen),...
            'SliderStep',[1/length(bin_chosen) 1/length(bin_chosen)],...
            'Value',bin_frame);
        updateAll();
    end

    function CB_slider_roi(src,~)
        roi_y=pic_h-floor(get(src,'Value'))+1;
        updateRoi(roi_y);
        set(edit_roi,'String',roi_y);
    end

    function CB_button_close(~,~)
        act=0;
        close 100;
    end
    
    function CB_play(src,~,speed,button_str)
        if(act==0)
            if(speed==2)
                speed=fwd_speed;
            end
            set(src,'String','Stop');
            act=1;
            while (act==1)
                pause(0.03);
                bin_frame=bin_frame+speed;
                if(bin_frame>length(bin_chosen))
                    bin_frame=bin_frame-length(bin_chosen);
                end
                set(slider_frame,'Value',bin_frame);
                current_fr=bin_chosen(bin_frame);
                updateAll();
            end
            act=0;
            set(src,'String',button_str)         
        end
        if(act==1)
            act=0;
        end
    end

    function CB_slider_frame(src,~)
        bin_frame=round(get(src,'Value'));
        current_fr=bin_chosen(bin_frame);
        updateAll();
    end
    

%% GENERAL FUNCTIONS
    function lungcalc()
        lungline=squeeze(sum(combmf(roi_y,:,:),2));
        lungsm=smooth(lungline,50);
        lung_adj=lungline-lungsm;
        [max_pks,max_pks_frame]=CTData.getPeaks(lung_adj,peakdst,0.1);
        [min_pks,min_pks_frame]=CTData.getPeaks(-lung_adj,peakdst,0.1);
        subts=(0:minExtend);
        min_pks_frame(1)=[];
        min_pks_frame=reshape(repmat(min_pks_frame,[1 minExtend+1])-subts,[size(min_pks_frame,1)*(minExtend+1) 1]);
        min_pks_frame=min_pks_frame(min_pks_frame>0);
        min_pks=lung_adj(min_pks_frame);
        minL=min(lung_adj);
        maxL=max(lung_adj);
    end

    function lungplot()
        axes(ax_lung_zoom);
            plot(ax_lung_zoom,lung_adj);
            hold on;
            scatter(max_pks_frame,max_pks);
            scatter(min_pks_frame,min_pks);
            hold off;
            line_frame=line([current_fr,current_fr],[minL_start, maxL_start]);
            set(line_frame,'Color','red');
            set(line_frame,'x',[current_fr, current_fr]);
            xlim(ax_lung_zoom,[current_fr-70 current_fr+70]);   
            set(ax_lung_zoom,'ylim',[minL_start maxL_start]);
    
        axes(ax_lung_over);
            plot(ax_lung_over,lung_adj); 
            line_frame_ov=line([current_fr,current_fr],[minL_start, maxL_start]);
            set(line_frame_ov,'x',[current_fr, current_fr]);
            set(line_frame_ov,'Color','red');  
            set(ax_lung_over,'ylim',[minL_start maxL_start]);
    end

    function updateRoi(roi_y)
        lungcalc();
        set(roi_line,'y',[roi_y,roi_y]);
        lungplot();    
    end

    function updateAll()
        %suptitle(sprintf('Current frame: %d / %d',current_fr,length(combmf)));
        set(supt,'String',sprintf('Current frame: %d / %d',current_fr,length(combmf)));
        set(single_img,'CData',combmf(:,:,current_fr));
        %plot(lung_adj((current_fr-50):(current_fr+50)));
        
        axes(ax_lung_zoom);
        set(line_frame,'x',[current_fr, current_fr]);
        xlim(ax_lung_zoom,[current_fr-70 current_fr+70]);
        axes(ax_lung_over);
        set(line_frame_ov,'x',[current_fr, current_fr]);
        set(edit_frame,'String',sprintf('%d',current_fr));
    end

end
