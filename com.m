classdef com <handle
    properties(Access=public)
        established=0
        WorkerQueue
        slice_nr
        sel_axis
    end
    
    methods
        function objc=com
        end
        
        function objc=createWorkerQueue(objc,queuetocreate)
            objc.established=1;
            objc.WorkerQueue=queuetocreate;
        end
    end
    
end

