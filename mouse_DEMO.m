%% initialization
tmp = matlab.desktop.editor.getActive;
cd(fileparts(tmp.Filename));
clear  tmp;
addpath('./custom_utilities')
%%
% Add tolbox folders
addpath('./Algorithms');
addpath('./Utilities');
addpath('./Utilities/Quality_measures');
addpath(genpath('./Test_data'));

% different arch versions
if ispc
    if ~isempty(strfind(computer('arch'),'64'))
        addpath('./Mex_files/win64');
    else
        addpath('./Mex_files/win32');
    end
elseif ismac
    if ~isempty(strfind(computer('arch'),'64'))
        addpath('./Mex_files/mac64');
    else
        addpath('./Mex_files/mac32');
    end
else
    if ~isempty(strfind(computer('arch'),'64'))
        addpath('./Mex_files/linux64');
    else
        addpath('./Mex_files/linux32');
    end
end
    
%addpath('./Demos');

% Perceptually uniform colormaps
% Add third party tools from FEX
%addpath('./Third_party_tools/arrow3d'); % 3D shepp-Logan
%addpath('./Third_party_tools/sec2hours');
%addpath('./Third_party_tools/readMHD');

if ispc
    if ~isempty(strfind(computer('arch'),'64'))
        addpath('./Mex_files/win64');
    else
        addpath('./Mex_files/win32');
    end
elseif ismac
    if ~isempty(strfind(computer('arch'),'64'))
        addpath('./Mex_files/mac64');
    else
        addpath('./Mex_files/mac32');
    end
else
    if ~isempty(strfind(computer('arch'),'64'))
        addpath('./Mex_files/linux64');
    else
        addpath('./Mex_files/linux32');
    end
end
%% load projection data
for part=1:16
    data(part)=CTData.loadData(part-1,'./../data/');
end

%%  prepare projection data
combmf=cat(3,data.MF);
clear data;
global min_pks_frame;
global max_pks_frame;

%% analzye projection data (set up gating)
projectionGUI(combmf);
%%
save('wholeobject.mat','test','-v7.3');


%%
figure(20);

%%
min_pks_frame(1:5)=[];
%%
minmouse=single(combmf(:,:,min_pks_frame));
anglesMin=double(min_pks_frame./size(combmf,3)*2*pi)';

%%
maxmouse=single(combmf(:,:,max_pks_frame));
anglesMax=double(max_pks_frame./size(combmf,3)*2*pi)';

%%
fullmouse=single(combmf);
%%

anglesFull=double(linspace(1/size(combmf,3),1,size(combmf,3))*2*pi);
%%
addpath('./TIGRE');
addpath('./TIGRE/utilities');
addpath('./TIGRE/Algorithms')
geo.DSD = 265;                             % Distance Source Detector      (mm)
geo.DSO = 52;                             % Distance Source Origin        (mm)
% Detector parameters
geo.nDetector=[512; 512];					% number of pixels              (px)
geo.dDetector=[0.254; 0.254]; 					% size of each pixel            (mm)
geo.sDetector=geo.nDetector.*geo.dDetector; % total size of the detector    (mm)
% Image parameters
geo.nVoxel=[512;512;512];                   % number of voxels              (vx)
geo.sVoxel=[20.43;20.43;20.43];                   % total size of the image       (mm)
geo.dVoxel=geo.sVoxel./geo.nVoxel;          % size of each voxel            (mm)
% Offsets
geo.offOrigin =[0;0;0];                     % Offset of image from origin   (mm)     

% detector (rotation axis offset for mouse #2 (Oct. 2018) is -0.57 in x!!!)
geo.offDetector=[-0.57;0];                   % Offset of Detector            (mm)


% Auxiliary 
geo.accuracy=0.5;                           % Accuracy of FWD proj          (vx/sample)
%%
zeroadd=single(zeros(1024,256,79));
maxmouse2=cat(2,maxmouse2,zeroadd);
%%
cd('TIGRE');
InitTIGRE;
%minFDK=FDK(minmouse,geo,anglesMin);
%minMLEM=MLEM(minmouse,geo,angles,75);
%maxMLEM=MLEM(maxmouse,geo,anglesMax,50);
maxFDK=FDK(maxmouse,geo,anglesMax);
%fullFDK=FDK(fullmouse,geo,anglesFull);
%minSARTTV=SART_TV(minmouse,geo,angles,10,'verbose',1,'init','image','initimg',minFDK);
cd('..');

%%
minFDK_norm=minFDK-min(min(min(minFDK)));
mouse2Min=slicesGUI(minFDK_norm);
mouse2Min=mouse2Min.startslicesGUI();

%%
maxFDK_norm=maxFDK-min(min(min(maxFDK)));
mouse2Max=slicesGUI(maxFDK_norm);
mouse2Max=mouse2Max.startslicesGUI([200,700]);

%%
fullFDK_norm=fullFDK-min(min(min(fullFDK)));
mouse2Full=slicesGUI(fullFDK_norm);
mouse2Full=mouse2Full.startslicesGUI();

%%
roi_tmp=mouse2Full.roi;
mous2withR=slicesGUI(fullFDK_norm,'roi',roi_tmp);
%%
mous2withR=mous2withR.startslicesGUI();
