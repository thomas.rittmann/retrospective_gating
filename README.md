# RetrospeCT: A Matlab based graphical user interface for micro-CT analysis.

## Executable
To allow running the software without an installed Matlab environment or a Matlab licence, a Windows executable is compiled and provided with an installer including all runtime dependencies.
  
The latest compiled executable and the corresponding runtime installer can be downloaded [here](https://owncloud.gwdg.de/index.php/s/Q7WAlcP2pmnqAsq).       
Access to the password protected download is granted upon request. 